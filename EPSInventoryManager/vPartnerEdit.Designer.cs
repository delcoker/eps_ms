﻿namespace EPSInventoryManager
{
    partial class vPartnerEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPartnerName = new System.Windows.Forms.TextBox();
            this.txtPartnerCity = new System.Windows.Forms.TextBox();
            this.txtPartnerEmail = new System.Windows.Forms.TextBox();
            this.txtPartnerPhone = new System.Windows.Forms.TextBox();
            this.dgvEditPartner = new System.Windows.Forms.DataGridView();
            this.txtPartnerRemarks = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.cbxPartnerStatus = new MetroFramework.Controls.MetroComboBox();
            this.cbxPartnerType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.cbxPartnerCountry = new MetroFramework.Controls.MetroComboBox();
            this.btnSaveEditPartner = new MetroFramework.Controls.MetroButton();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtPartnerAddress = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditPartner)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPartnerName
            // 
            this.txtPartnerName.Location = new System.Drawing.Point(18, 54);
            this.txtPartnerName.Name = "txtPartnerName";
            this.txtPartnerName.Size = new System.Drawing.Size(252, 20);
            this.txtPartnerName.TabIndex = 0;
            // 
            // txtPartnerCity
            // 
            this.txtPartnerCity.Location = new System.Drawing.Point(18, 275);
            this.txtPartnerCity.Name = "txtPartnerCity";
            this.txtPartnerCity.Size = new System.Drawing.Size(252, 20);
            this.txtPartnerCity.TabIndex = 4;
            // 
            // txtPartnerEmail
            // 
            this.txtPartnerEmail.Location = new System.Drawing.Point(307, 159);
            this.txtPartnerEmail.Name = "txtPartnerEmail";
            this.txtPartnerEmail.Size = new System.Drawing.Size(246, 20);
            this.txtPartnerEmail.TabIndex = 7;
            // 
            // txtPartnerPhone
            // 
            this.txtPartnerPhone.Location = new System.Drawing.Point(18, 107);
            this.txtPartnerPhone.Name = "txtPartnerPhone";
            this.txtPartnerPhone.Size = new System.Drawing.Size(252, 20);
            this.txtPartnerPhone.TabIndex = 1;
            // 
            // dgvEditPartner
            // 
            this.dgvEditPartner.AllowUserToAddRows = false;
            this.dgvEditPartner.AllowUserToDeleteRows = false;
            this.dgvEditPartner.AllowUserToOrderColumns = true;
            this.dgvEditPartner.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvEditPartner.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvEditPartner.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvEditPartner.Location = new System.Drawing.Point(33, 386);
            this.dgvEditPartner.Margin = new System.Windows.Forms.Padding(2);
            this.dgvEditPartner.Name = "dgvEditPartner";
            this.dgvEditPartner.ReadOnly = true;
            this.dgvEditPartner.RowTemplate.Height = 24;
            this.dgvEditPartner.Size = new System.Drawing.Size(1181, 269);
            this.dgvEditPartner.TabIndex = 100;
            this.dgvEditPartner.TabStop = false;
            this.dgvEditPartner.Click += new System.EventHandler(this.dgvEditPartner_Click);
            // 
            // txtPartnerRemarks
            // 
            // 
            // 
            // 
            this.txtPartnerRemarks.CustomButton.Image = null;
            this.txtPartnerRemarks.CustomButton.Location = new System.Drawing.Point(176, 2);
            this.txtPartnerRemarks.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtPartnerRemarks.CustomButton.Name = "";
            this.txtPartnerRemarks.CustomButton.Size = new System.Drawing.Size(67, 67);
            this.txtPartnerRemarks.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPartnerRemarks.CustomButton.TabIndex = 1;
            this.txtPartnerRemarks.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPartnerRemarks.CustomButton.UseSelectable = true;
            this.txtPartnerRemarks.CustomButton.Visible = false;
            this.txtPartnerRemarks.Lines = new string[0];
            this.txtPartnerRemarks.Location = new System.Drawing.Point(307, 203);
            this.txtPartnerRemarks.Margin = new System.Windows.Forms.Padding(2);
            this.txtPartnerRemarks.MaxLength = 32767;
            this.txtPartnerRemarks.Multiline = true;
            this.txtPartnerRemarks.Name = "txtPartnerRemarks";
            this.txtPartnerRemarks.PasswordChar = '\0';
            this.txtPartnerRemarks.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPartnerRemarks.SelectedText = "";
            this.txtPartnerRemarks.SelectionLength = 0;
            this.txtPartnerRemarks.SelectionStart = 0;
            this.txtPartnerRemarks.ShortcutsEnabled = true;
            this.txtPartnerRemarks.Size = new System.Drawing.Size(246, 72);
            this.txtPartnerRemarks.TabIndex = 8;
            this.txtPartnerRemarks.UseSelectable = true;
            this.txtPartnerRemarks.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPartnerRemarks.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(307, 85);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(46, 19);
            this.metroLabel10.TabIndex = 83;
            this.metroLabel10.Text = "Status:";
            // 
            // cbxPartnerStatus
            // 
            this.cbxPartnerStatus.FormattingEnabled = true;
            this.cbxPartnerStatus.ItemHeight = 23;
            this.cbxPartnerStatus.Location = new System.Drawing.Point(307, 106);
            this.cbxPartnerStatus.Margin = new System.Windows.Forms.Padding(2);
            this.cbxPartnerStatus.Name = "cbxPartnerStatus";
            this.cbxPartnerStatus.Size = new System.Drawing.Size(246, 29);
            this.cbxPartnerStatus.TabIndex = 6;
            this.cbxPartnerStatus.UseSelectable = true;
            // 
            // cbxPartnerType
            // 
            this.cbxPartnerType.FormattingEnabled = true;
            this.cbxPartnerType.ItemHeight = 23;
            this.cbxPartnerType.Location = new System.Drawing.Point(307, 54);
            this.cbxPartnerType.Margin = new System.Windows.Forms.Padding(2);
            this.cbxPartnerType.Name = "cbxPartnerType";
            this.cbxPartnerType.Size = new System.Drawing.Size(246, 29);
            this.cbxPartnerType.TabIndex = 5;
            this.cbxPartnerType.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(307, 36);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(86, 19);
            this.metroLabel9.TabIndex = 82;
            this.metroLabel9.Text = "Partner Type:";
            // 
            // cbxPartnerCountry
            // 
            this.cbxPartnerCountry.FormattingEnabled = true;
            this.cbxPartnerCountry.ItemHeight = 23;
            this.cbxPartnerCountry.Location = new System.Drawing.Point(18, 222);
            this.cbxPartnerCountry.Margin = new System.Windows.Forms.Padding(2);
            this.cbxPartnerCountry.Name = "cbxPartnerCountry";
            this.cbxPartnerCountry.Size = new System.Drawing.Size(252, 29);
            this.cbxPartnerCountry.TabIndex = 3;
            this.cbxPartnerCountry.UseSelectable = true;
            // 
            // btnSaveEditPartner
            // 
            this.btnSaveEditPartner.Location = new System.Drawing.Point(453, 279);
            this.btnSaveEditPartner.Margin = new System.Windows.Forms.Padding(2);
            this.btnSaveEditPartner.Name = "btnSaveEditPartner";
            this.btnSaveEditPartner.Size = new System.Drawing.Size(100, 38);
            this.btnSaveEditPartner.TabIndex = 9;
            this.btnSaveEditPartner.Text = "&Save";
            this.btnSaveEditPartner.UseSelectable = true;
            this.btnSaveEditPartner.Click += new System.EventHandler(this.btnSaveEditPartner_Click);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(307, 182);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(62, 19);
            this.metroLabel7.TabIndex = 81;
            this.metroLabel7.Text = "Remarks:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(307, 137);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(44, 19);
            this.metroLabel6.TabIndex = 80;
            this.metroLabel6.Text = "Email:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(18, 85);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(49, 19);
            this.metroLabel5.TabIndex = 79;
            this.metroLabel5.Text = "Phone:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(18, 201);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(59, 19);
            this.metroLabel4.TabIndex = 78;
            this.metroLabel4.Text = "Country:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(18, 253);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(34, 19);
            this.metroLabel3.TabIndex = 77;
            this.metroLabel3.Text = "City:";
            // 
            // txtPartnerAddress
            // 
            // 
            // 
            // 
            this.txtPartnerAddress.CustomButton.Image = null;
            this.txtPartnerAddress.CustomButton.Location = new System.Drawing.Point(212, 1);
            this.txtPartnerAddress.CustomButton.Margin = new System.Windows.Forms.Padding(2);
            this.txtPartnerAddress.CustomButton.Name = "";
            this.txtPartnerAddress.CustomButton.Size = new System.Drawing.Size(39, 39);
            this.txtPartnerAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPartnerAddress.CustomButton.TabIndex = 1;
            this.txtPartnerAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPartnerAddress.CustomButton.UseSelectable = true;
            this.txtPartnerAddress.CustomButton.Visible = false;
            this.txtPartnerAddress.Lines = new string[0];
            this.txtPartnerAddress.Location = new System.Drawing.Point(18, 158);
            this.txtPartnerAddress.Margin = new System.Windows.Forms.Padding(2);
            this.txtPartnerAddress.MaxLength = 32767;
            this.txtPartnerAddress.Multiline = true;
            this.txtPartnerAddress.Name = "txtPartnerAddress";
            this.txtPartnerAddress.PasswordChar = '\0';
            this.txtPartnerAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPartnerAddress.SelectedText = "";
            this.txtPartnerAddress.SelectionLength = 0;
            this.txtPartnerAddress.SelectionStart = 0;
            this.txtPartnerAddress.ShortcutsEnabled = true;
            this.txtPartnerAddress.Size = new System.Drawing.Size(252, 41);
            this.txtPartnerAddress.TabIndex = 2;
            this.txtPartnerAddress.UseSelectable = true;
            this.txtPartnerAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPartnerAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(18, 137);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(59, 19);
            this.metroLabel2.TabIndex = 76;
            this.metroLabel2.Text = "Address:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(18, 32);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(109, 19);
            this.metroLabel1.TabIndex = 75;
            this.metroLabel1.Text = "Company Name:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtPartnerEmail);
            this.groupBox1.Controls.Add(this.txtPartnerName);
            this.groupBox1.Controls.Add(this.btnSaveEditPartner);
            this.groupBox1.Controls.Add(this.txtPartnerRemarks);
            this.groupBox1.Controls.Add(this.txtPartnerCity);
            this.groupBox1.Controls.Add(this.metroLabel10);
            this.groupBox1.Controls.Add(this.txtPartnerPhone);
            this.groupBox1.Controls.Add(this.cbxPartnerStatus);
            this.groupBox1.Controls.Add(this.cbxPartnerType);
            this.groupBox1.Controls.Add(this.cbxPartnerCountry);
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.metroLabel7);
            this.groupBox1.Controls.Add(this.metroLabel5);
            this.groupBox1.Controls.Add(this.metroLabel6);
            this.groupBox1.Controls.Add(this.metroLabel4);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.metroLabel3);
            this.groupBox1.Controls.Add(this.metroLabel2);
            this.groupBox1.Controls.Add(this.txtPartnerAddress);
            this.groupBox1.Location = new System.Drawing.Point(33, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(573, 330);
            this.groupBox1.TabIndex = 85;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Partner details";
            // 
            // vPartnerEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 673);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvEditPartner);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MinimumSize = new System.Drawing.Size(632, 531);
            this.Name = "vPartnerEdit";
            this.Padding = new System.Windows.Forms.Padding(10, 60, 10, 10);
            this.Text = "Edit Partner";
            this.Load += new System.EventHandler(this.vPartnerEdit_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEditPartner)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtPartnerName;
        private System.Windows.Forms.TextBox txtPartnerCity;
        private System.Windows.Forms.TextBox txtPartnerEmail;
        private System.Windows.Forms.TextBox txtPartnerPhone;
        private System.Windows.Forms.DataGridView dgvEditPartner;
        private MetroFramework.Controls.MetroTextBox txtPartnerRemarks;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox cbxPartnerStatus;
        private MetroFramework.Controls.MetroComboBox cbxPartnerType;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox cbxPartnerCountry;
        private MetroFramework.Controls.MetroButton btnSaveEditPartner;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtPartnerAddress;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}