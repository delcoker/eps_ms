﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace EPSInventoryManager
{
    public partial class frmInventory : MetroForm
    {
        private DataSet itemDs;

        public frmInventory()
        {
            InitializeComponent();
            loadComboBoxes();
        }

        private void loadItemsIntoDataGrid()
        {
            DataSet ds = new DataSet();
            cItem it = new cItem();

            try
            {
                it.ItemId = "0";
                ds = it.ItemQuantityGet();

                metrodgvItem.DataSource = ds.Tables[0];

                metrodgvItem.Columns["category_id"].HeaderText = "Catgory ID";
                metrodgvItem.Columns["category_id"].Visible = false;
                metrodgvItem.Columns["item_id"].HeaderText = "Item ID";
                metrodgvItem.Columns["item_id"].Visible = false;
                metrodgvItem.Columns["item_name"].HeaderText = "Name";
                metrodgvItem.Columns["item_price"].HeaderText = "Price";
                metrodgvItem.Columns["item_quantity"].HeaderText = "Quantity";
                metrodgvItem.Columns["category_name"].HeaderText = "Category Name";
                //                dgvItemCategory.Columns["category_name"].Width = 200;
                metrodgvItem.Columns["minimum_stock_level"].HeaderText = "Min Stock Level";
                //metrodgvItem.Columns["vendor_id"].Visible = false;
                metrodgvItem.Columns["description_"].HeaderText = "Description";
                metrodgvItem.Columns["status_"].HeaderText = "Status";
                metrodgvItem.Columns["created_at"].HeaderText = "Date Created";
                //                dgvItemCategory.Columns["created_at"].Width = 200;
                metrodgvItem.Columns["updated_at"].HeaderText = "Last Modified";
                //                dgvItemCategory.Columns["updated_at"].Width = 200;

                //                for(int i = 0; i < dgvItemCategory.ColumnCount; i++){
                //                dgvItemCategory.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                //                int widthCol = dgvItemCategory.Columns[i].Width;
                //                dgvItemCategory.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                //                dgvItemCategory.Columns[i].Width = widthCol;
                //                }

                //metrodgvItem.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                //                dgvItemCategory.AllowUserToResizeColumns = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
            //pEdt.Enabled = false;
        }

        private void frmInventory_Load(object sender, EventArgs e)
        {
            loadItemsIntoDataGrid();
        }
        //This method loads data into the controls on the Inventory form
        public void loadComboBoxes()
        {
            SqlCo.loadLocation(cbxInvLocation);
            SqlCo.loadStatus(cbxInvStatus);
            SqlCo.loadTransactionType(cbxTransactionType);
            SqlCo.loadPurchaseType(cbxPurchaseType);

            loadItemsIntoDataGrid();
            loadItemsIntoComboBox(); //  #enable after i get itemGEt prc
            loadCategories();
            loadBusinessPartner();

        }

        private void loadItemsIntoComboBox()
        {
            itemDs = new DataSet();
            cItem it = new cItem();

            itemDs = it.ItemGet();

            cbxItems.DataSource = itemDs.Tables[0].DefaultView;
            cbxItems.ValueMember = "item_id";
            cbxItems.DisplayMember = "item_name";
        }

        private void loadBusinessPartner()
        {

            //        business_partnerId,
            //business_partnerName,
            DataSet ds = new DataSet();
            cBusinessPartner bp = new cBusinessPartner();
            bp.Business_partnerId = "0";
            ds = bp.SupplierGet();

            cbxBusinessPartner.DataSource = ds.Tables[0].DefaultView;
            cbxBusinessPartner.ValueMember = "business_partnerId";
            cbxBusinessPartner.DisplayMember = "business_partnerName";
        }

        private void loadCategories()
        {
            // now to load data from the database into the combo box --ok
            // You need your dataset object
            DataSet ds = new DataSet();

            // you category object
            cItemCategory catInv = new cItemCategory();
            // from the stored procedure, to get all item when fetching the category id should be 0
            catInv.CategoryId = "0";
            // now just call the function to fetch data and store what it gets in the dataset
            ds = catInv.CategoryGet();

            // now bind the data to the combo box --ok
            cbxInvCategory.DataSource = ds.Tables[0].DefaultView;
            cbxInvCategory.ValueMember = "category_id";
            cbxInvCategory.DisplayMember = "category_name";
        }

        private void btnCancel_Click(object sender, EventArgs e)//Changed the name of the button to btnCancel
        {
            this.Close();
        }

        private void btnTopUp_Click(object sender, EventArgs e)//changed the name of the button to btnTopUp
        {

            //Check for Expiry date difference between Manufacture date
            if (dateTimePicker2.Value < dateTimePicker1.Value)
            {
                //MetroFramework.MetroMessageBox.Show(this, "Expiry date should be greater than manufacture date.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Expiry Date should be greater than Manufacture Date.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //Check Quantity to be greater than or equal to 0
            if (numQtyInvAdd.Value <= 0)
            {
                //MetroFramework.MetroMessageBox.Show(this, "Quantity should be greater than 0.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                MessageBox.Show("Quantity should be greater than 0.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            cItem it = new cItem();
            it.ItemId = cbxItems.SelectedValue.ToString();
            it.Quantity = Convert.ToInt32(numQtyInvAdd.Value);
            if (!it.ItemQuantitySave())
            {
                MessageBox.Show("Could not save into Items");
            }
            //TODO: Get "Logged in User" from Users table
            cInventoryTransaction invTran = new cInventoryTransaction("0", "user1", cbxPurchaseType.SelectedValue.ToString(), cbxBusinessPartner.SelectedValue.ToString(),
                cbxTransactionType.SelectedValue.ToString(), txtWayBil.Text, metroTextBox8.Text, true);
            if (!invTran.InventorySave())
            {
                return;
            }



            cInventoryTransactionDetail invTranDetail = new cInventoryTransactionDetail("0", invTran.GeneratedId, it.ItemId, it.Quantity,
                Convert.ToDecimal(numPrice.Value), dateTimePicker1.Value.ToString(),
                dateTimePicker2.Value.ToString(), txtPart.Text, /*cbxBusinessPartner.SelectedValue.ToString(),*/ cbxInvLocation.SelectedValue.ToString(), metroTextBox8.Text, true);
            if (!invTranDetail.InventoryTransactionDetailSave())
            {
                MessageBox.Show("Could not save.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {

                MessageBox.Show("Saved Successfully.", "Saving Inventory", MessageBoxButtons.OK, MessageBoxIcon.Information);
                numQtyInvAdd.Value = 0;

                //Reload inventory datagridview
                frmInventory_Load(sender, e);


            }

        }

        // populate item details

        private void cbxItems_SelectedIndexChanged(object sender, EventArgs e)
        {

            cItem it = new cItem();
            it.ItemId = cbxItems.SelectedValue.ToString();


            if (it.ItemId.Length > 3)
            {
                foreach (DataRow dr in itemDs.Tables[0].Rows)
                {
                    if (dr["item_id"].ToString() == it.ItemId)
                    {
                        cbxInvCategory.SelectedValue = dr["category_id"].ToString();
                        numMinStockLevel.Value = Convert.ToInt32(dr["minimum_stock_level"]);
                        numPrice.Value = Convert.ToDecimal(dr["item_price"]);
                        cbxInvStatus.SelectedValue = dr["status_"].ToString();
                        txtDescription.Text = dr["description_"].ToString();
                    }
                }

            }

            //DataSet ds = loggedUser.UserGetWithEmail();
            //loggedUser.UserId = (ds.Tables[0].Rows[0]["user_id"].ToString());
            //loggedUser.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
            //loggedUser.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
            //loggedUser.OtherName = ds.Tables[0].Rows[0]["other_name"].ToString();
            //loggedUser.PostalAddress = ds.Tables[0].Rows[0]["postal_address"].ToString();
            //loggedUser.PrimaryPhone = ds.Tables[0].Rows[0]["primary_phone"].ToString();

        }

        private void metrodgvItem_Click(object sender, EventArgs e)
        {

        }

        private void cbxInvCategory_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //Enable expiry date selection if item has expiry date.
        //private void checkBox1_CheckedChanged(object sender, EventArgs e)
        //{

        //    dateTimePicker1.Enabled = true;
        //    dateTimePicker2.Enabled = true;
        //}

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void chkExpiry_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkExpiry.Checked)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker2.Enabled = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker2.Enabled = false;
            }

        }


        //Disable expiry date selection if item has no expiry date.




        ////This method loads data into the Items list control
        //public DataSet ItemGet()
        //{
        //    cItem item = new cItem();
        //    SqlCo sqls;
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemsGet";


        //    sqls.query("@item_id", item.ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}
    }
}
