﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace EPSInventoryManager
{
    public partial class vCategoryEdit : MetroForm
    {
        public vCategoryEdit()
        {
            InitializeComponent();

            SqlCo.loadStatus(cbxCatStatus);

        }

        private void vCategoryEdit_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'ePS_ProjectDataSet.category' table. You can move, or remove it, as needed.
            //this.categoryTableAdapter.Fill(this.ePS_ProjectDataSet.category);

            DataSet ds = new DataSet();
            cItemCategory cat = new cItemCategory();

            try
            {
                cat.CategoryId = "0";

                ds = cat.CategoryGet();

                dgvCategoryView.DataSource = ds.Tables[0];

                dgvCategoryView.Columns["category_id"].HeaderText = "Catgory ID";
                dgvCategoryView.Columns["category_id"].Visible = false;
                dgvCategoryView.Columns["category_name"].HeaderText = "Category Name";
                dgvCategoryView.Columns["remarks"].HeaderText = "Description";
                dgvCategoryView.Columns["status_"].HeaderText = "Status";
                dgvCategoryView.Columns["created_at"].HeaderText = "Date Created";
                dgvCategoryView.Columns["updated_at"].HeaderText = "Last Modified";


                dgvCategoryView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                //                dgvCategoryView.AllowUserToResizeColumns = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
            //pEdt.Enabled = false;
        }

        private void categorytableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }


        private void metroButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            cItemCategory cat = new cItemCategory();
            cat.CategoryId = dgvCategoryView["category_id", dgvCategoryView.CurrentCell.RowIndex].Value.ToString();
            cat.CategoryName = txtCategoryName.Text;
            cat.Remarks = txtCatRemarks.Text;
            cat.Status = cbxCatStatus.SelectedValue.ToString();

            if (!cat.CategorySave())
            {
                MessageBox.Show("Could not save");
                return;
            }
            else
            {
                MessageBox.Show("saved successfully.");
            }

            //dgvCategoryView.Refreshe();
            txtCatRemarks.Clear();
            vCategoryEdit_Load(sender, e);
        }

        private void dgvCategoryView_Click(object sender, EventArgs e)
        {
            //pEdt.Enabled = false;
            try
            {
                txtCategoryName.Text = (dgvCategoryView["category_name", dgvCategoryView.CurrentCell.RowIndex].Value.ToString());
                txtCatRemarks.Text = (dgvCategoryView["remarks", dgvCategoryView.CurrentCell.RowIndex].Value.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show("You have no items to view\n" + ex.Message);
            }
            cbxCatStatus.SelectedValue = (dgvCategoryView["status_", dgvCategoryView.CurrentCell.RowIndex].Value.ToString());
        }
    }

}
