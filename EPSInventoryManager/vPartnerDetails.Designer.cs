﻿namespace EPSInventoryManager
{
    partial class frmPartnerDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPartnerRemarks = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.cbxPartnerStatus = new MetroFramework.Controls.MetroComboBox();
            this.cbxPartnerType = new MetroFramework.Controls.MetroComboBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.cbxPartnerCountry = new MetroFramework.Controls.MetroComboBox();
            this.btnAddPartner = new MetroFramework.Controls.MetroButton();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.txtPartnerAddress = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.txtPartnerEmail = new System.Windows.Forms.TextBox();
            this.txtPartnerPhone = new System.Windows.Forms.TextBox();
            this.txtPartnerName = new System.Windows.Forms.TextBox();
            this.txtPartnerCity = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtPartnerRemarks
            // 
            // 
            // 
            // 
            this.txtPartnerRemarks.CustomButton.Image = null;
            this.txtPartnerRemarks.CustomButton.Location = new System.Drawing.Point(161, 2);
            this.txtPartnerRemarks.CustomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPartnerRemarks.CustomButton.Name = "";
            this.txtPartnerRemarks.CustomButton.Size = new System.Drawing.Size(67, 67);
            this.txtPartnerRemarks.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPartnerRemarks.CustomButton.TabIndex = 1;
            this.txtPartnerRemarks.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPartnerRemarks.CustomButton.UseSelectable = true;
            this.txtPartnerRemarks.CustomButton.Visible = false;
            this.txtPartnerRemarks.Lines = new string[0];
            this.txtPartnerRemarks.Location = new System.Drawing.Point(292, 270);
            this.txtPartnerRemarks.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPartnerRemarks.MaxLength = 32767;
            this.txtPartnerRemarks.Multiline = true;
            this.txtPartnerRemarks.Name = "txtPartnerRemarks";
            this.txtPartnerRemarks.PasswordChar = '\0';
            this.txtPartnerRemarks.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPartnerRemarks.SelectedText = "";
            this.txtPartnerRemarks.SelectionLength = 0;
            this.txtPartnerRemarks.SelectionStart = 0;
            this.txtPartnerRemarks.ShortcutsEnabled = true;
            this.txtPartnerRemarks.Size = new System.Drawing.Size(231, 72);
            this.txtPartnerRemarks.TabIndex = 8;
            this.txtPartnerRemarks.UseSelectable = true;
            this.txtPartnerRemarks.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPartnerRemarks.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(292, 131);
            this.metroLabel10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(46, 19);
            this.metroLabel10.TabIndex = 61;
            this.metroLabel10.Text = "Status:";
            // 
            // cbxPartnerStatus
            // 
            this.cbxPartnerStatus.FormattingEnabled = true;
            this.cbxPartnerStatus.ItemHeight = 23;
            this.cbxPartnerStatus.Location = new System.Drawing.Point(292, 150);
            this.cbxPartnerStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxPartnerStatus.Name = "cbxPartnerStatus";
            this.cbxPartnerStatus.Size = new System.Drawing.Size(231, 29);
            this.cbxPartnerStatus.TabIndex = 6;
            this.cbxPartnerStatus.UseSelectable = true;
            // 
            // cbxPartnerType
            // 
            this.cbxPartnerType.FormattingEnabled = true;
            this.cbxPartnerType.ItemHeight = 23;
            this.cbxPartnerType.Location = new System.Drawing.Point(292, 94);
            this.cbxPartnerType.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxPartnerType.Name = "cbxPartnerType";
            this.cbxPartnerType.Size = new System.Drawing.Size(231, 29);
            this.cbxPartnerType.TabIndex = 5;
            this.cbxPartnerType.UseSelectable = true;
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(292, 77);
            this.metroLabel9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(86, 19);
            this.metroLabel9.TabIndex = 60;
            this.metroLabel9.Text = "Partner Type:";
            // 
            // cbxPartnerCountry
            // 
            this.cbxPartnerCountry.FormattingEnabled = true;
            this.cbxPartnerCountry.ItemHeight = 23;
            this.cbxPartnerCountry.Location = new System.Drawing.Point(39, 270);
            this.cbxPartnerCountry.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxPartnerCountry.Name = "cbxPartnerCountry";
            this.cbxPartnerCountry.Size = new System.Drawing.Size(231, 29);
            this.cbxPartnerCountry.TabIndex = 3;
            this.cbxPartnerCountry.UseSelectable = true;
            // 
            // btnAddPartner
            // 
            this.btnAddPartner.Location = new System.Drawing.Point(427, 348);
            this.btnAddPartner.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAddPartner.Name = "btnAddPartner";
            this.btnAddPartner.Size = new System.Drawing.Size(96, 38);
            this.btnAddPartner.TabIndex = 9;
            this.btnAddPartner.Text = "&Add Partner";
            this.btnAddPartner.UseSelectable = true;
            this.btnAddPartner.Click += new System.EventHandler(this.btnAddPartner_Click);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(292, 251);
            this.metroLabel7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(62, 19);
            this.metroLabel7.TabIndex = 59;
            this.metroLabel7.Text = "Remarks:";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(292, 185);
            this.metroLabel6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(44, 19);
            this.metroLabel6.TabIndex = 55;
            this.metroLabel6.Text = "Email:";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(39, 131);
            this.metroLabel5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(49, 19);
            this.metroLabel5.TabIndex = 53;
            this.metroLabel5.Text = "Phone:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(39, 251);
            this.metroLabel4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(59, 19);
            this.metroLabel4.TabIndex = 52;
            this.metroLabel4.Text = "Country:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(39, 302);
            this.metroLabel3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(34, 19);
            this.metroLabel3.TabIndex = 50;
            this.metroLabel3.Text = "City:";
            // 
            // txtPartnerAddress
            // 
            // 
            // 
            // 
            this.txtPartnerAddress.CustomButton.Image = null;
            this.txtPartnerAddress.CustomButton.Location = new System.Drawing.Point(191, 1);
            this.txtPartnerAddress.CustomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPartnerAddress.CustomButton.Name = "";
            this.txtPartnerAddress.CustomButton.Size = new System.Drawing.Size(39, 39);
            this.txtPartnerAddress.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtPartnerAddress.CustomButton.TabIndex = 1;
            this.txtPartnerAddress.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtPartnerAddress.CustomButton.UseSelectable = true;
            this.txtPartnerAddress.CustomButton.Visible = false;
            this.txtPartnerAddress.Lines = new string[0];
            this.txtPartnerAddress.Location = new System.Drawing.Point(39, 204);
            this.txtPartnerAddress.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtPartnerAddress.MaxLength = 32767;
            this.txtPartnerAddress.Multiline = true;
            this.txtPartnerAddress.Name = "txtPartnerAddress";
            this.txtPartnerAddress.PasswordChar = '\0';
            this.txtPartnerAddress.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtPartnerAddress.SelectedText = "";
            this.txtPartnerAddress.SelectionLength = 0;
            this.txtPartnerAddress.SelectionStart = 0;
            this.txtPartnerAddress.ShortcutsEnabled = true;
            this.txtPartnerAddress.Size = new System.Drawing.Size(231, 41);
            this.txtPartnerAddress.TabIndex = 2;
            this.txtPartnerAddress.UseSelectable = true;
            this.txtPartnerAddress.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtPartnerAddress.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(39, 185);
            this.metroLabel2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(59, 19);
            this.metroLabel2.TabIndex = 47;
            this.metroLabel2.Text = "Address:";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(39, 77);
            this.metroLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(109, 19);
            this.metroLabel1.TabIndex = 45;
            this.metroLabel1.Text = "Company Name:";
            // 
            // txtPartnerEmail
            // 
            this.txtPartnerEmail.Location = new System.Drawing.Point(292, 207);
            this.txtPartnerEmail.Name = "txtPartnerEmail";
            this.txtPartnerEmail.Size = new System.Drawing.Size(231, 20);
            this.txtPartnerEmail.TabIndex = 7;
            // 
            // txtPartnerPhone
            // 
            this.txtPartnerPhone.Location = new System.Drawing.Point(39, 155);
            this.txtPartnerPhone.Name = "txtPartnerPhone";
            this.txtPartnerPhone.Size = new System.Drawing.Size(231, 20);
            this.txtPartnerPhone.TabIndex = 1;
            // 
            // txtPartnerName
            // 
            this.txtPartnerName.Location = new System.Drawing.Point(39, 98);
            this.txtPartnerName.Name = "txtPartnerName";
            this.txtPartnerName.Size = new System.Drawing.Size(231, 20);
            this.txtPartnerName.TabIndex = 0;
            // 
            // txtPartnerCity
            // 
            this.txtPartnerCity.Location = new System.Drawing.Point(39, 324);
            this.txtPartnerCity.Name = "txtPartnerCity";
            this.txtPartnerCity.Size = new System.Drawing.Size(231, 20);
            this.txtPartnerCity.TabIndex = 4;
            // 
            // frmPartnerDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(563, 410);
            this.Controls.Add(this.txtPartnerCity);
            this.Controls.Add(this.txtPartnerName);
            this.Controls.Add(this.txtPartnerPhone);
            this.Controls.Add(this.txtPartnerEmail);
            this.Controls.Add(this.txtPartnerRemarks);
            this.Controls.Add(this.metroLabel10);
            this.Controls.Add(this.cbxPartnerStatus);
            this.Controls.Add(this.cbxPartnerType);
            this.Controls.Add(this.metroLabel9);
            this.Controls.Add(this.cbxPartnerCountry);
            this.Controls.Add(this.btnAddPartner);
            this.Controls.Add(this.metroLabel7);
            this.Controls.Add(this.metroLabel6);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroLabel4);
            this.Controls.Add(this.metroLabel3);
            this.Controls.Add(this.txtPartnerAddress);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(563, 410);
            this.MinimumSize = new System.Drawing.Size(563, 410);
            this.Name = "frmPartnerDetails";
            this.Padding = new System.Windows.Forms.Padding(10, 60, 10, 10);
            this.Resizable = false;
            this.Text = "Partner Details";
            this.Load += new System.EventHandler(this.frmPartnerDetails_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox txtPartnerRemarks;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroComboBox cbxPartnerStatus;
        private MetroFramework.Controls.MetroComboBox cbxPartnerType;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroComboBox cbxPartnerCountry;
        private MetroFramework.Controls.MetroButton btnAddPartner;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox txtPartnerAddress;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.TextBox txtPartnerEmail;
        private System.Windows.Forms.TextBox txtPartnerPhone;
        private System.Windows.Forms.TextBox txtPartnerName;
        private System.Windows.Forms.TextBox txtPartnerCity;
    }
}