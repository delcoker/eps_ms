﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace EPSInventoryManager
{
    public partial class vItemEdit : MetroForm
    {
        public vItemEdit()
        {
            InitializeComponent();
            SqlCo.loadStatus(cbxAddItemStatus);
            loadCategory();
        }


        public void loadCategory()
        {
            // now to load data from the database into the combo box --ok
            // You need your dataset object
            DataSet ds = new DataSet();

            // you category object
            cItemCategory cat = new cItemCategory();

            // from the stored procedure, to get all item when fetching the category id should be 0
            cat.CategoryId = "0";

            // now just call the function to fetch data and store what it gets in the dataset
            ds = cat.CategoryGet();

            // now bind the data to the combo box --ok
            cbxItemCategory.DataSource = ds.Tables[0].DefaultView;
            cbxItemCategory.ValueMember = "category_id";
            cbxItemCategory.DisplayMember = "category_name";
            cbxItemCategory.SelectedIndex = 0;
        }

        private void vItemEdit_Load(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            cItem itm = new cItem();

            try
            {
                itm.ItemId = "0";

                ds = itm.ItemsGet();

                dgvItemEdit.DataSource = ds.Tables[0];
                dgvItemEdit.AllowUserToResizeColumns = true;
                dgvItemEdit.Columns["item_id"].HeaderText = "Item ID";
                dgvItemEdit.Columns["item_id"].Visible = false;
                dgvItemEdit.Columns["item_name"].HeaderText = "Item Name";
                dgvItemEdit.Columns["category_id"].HeaderText = "Category ID";
                dgvItemEdit.Columns["category_id"].Visible = false;
                dgvItemEdit.Columns["category_name"].HeaderText = "Category";
                dgvItemEdit.Columns["minimum_stock_level"].HeaderText = "Min. Stock Level";
                dgvItemEdit.Columns["description_"].HeaderText = "Description";
                dgvItemEdit.Columns["status_"].HeaderText = "Status";
                dgvItemEdit.Columns["created_at"].HeaderText = "Created at";
                dgvItemEdit.Columns["updated_at"].HeaderText = "Updated at";
                dgvItemEdit.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                dgvItemEdit.AllowUserToResizeColumns = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
            //pEdt.Enabled = false;
        }

        private void btnCancelAddItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        //private void dgvItemEdit_Click(object sender, DataGridViewCellEventArgs e)
        //{
        //    try
        //    {
        //        txtAddItemName.Text = dgvItemEdit["item_name", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show("You have no items to view\n." + ex.Message);
        //    }
        //    cbxAddItemStatus.SelectedValue = dgvItemEdit["status_", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
        //    cbxItemCategory.SelectedValue = dgvItemEdit["category_id", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
        //    txtItemDescription.Text = dgvItemEdit["description_", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
        //    txtUnitMes.Text = dgvItemEdit["unit_of_measure", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
        //}
        private void btnAddItem_Click(object sender, EventArgs e)
        {
            cItem itm = new cItem();
            if (txtAddItemName.Text == "")
            {
                MessageBox.Show("Please select an Item Name.", "Adding Item.", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //is this ok? sure
                return;
            }
            itm.ItemId = dgvItemEdit["Item_id", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
            itm.ItemName = txtAddItemName.Text;
            itm.Description = txtItemDescription.Text;
            itm.Units = txtUnitMes.Text;
            itm.CategoryId = cbxItemCategory.SelectedValue.ToString();
            itm.MinimumStockLevel = Convert.ToInt32(nupMinStockLevel.Value);
            itm.Status_ = cbxAddItemStatus.SelectedValue.ToString();
            itm.Price = Convert.ToInt32(numPrice.Value);
            if (!itm.ItemSave())
            {
                MessageBox.Show("Could not save.");
                return;
            }
            else
            {
                MessageBox.Show("Saved successfully.");
                txtAddItemName.Text = "";
                txtAddItemName.Focus();
            }
            vItemEdit_Load(sender, e);
        }

        private void dgvItemEdit_Click(object sender, EventArgs e)
        {
            try
            {
                txtAddItemName.Text = dgvItemEdit["item_name", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("You have no items to view\n." + ex.Message);
            }
            cbxAddItemStatus.SelectedValue = dgvItemEdit["status_", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
            cbxItemCategory.SelectedValue = dgvItemEdit["category_id", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
            txtItemDescription.Text = dgvItemEdit["description_", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
            txtUnitMes.Text = dgvItemEdit["unit_of_measure", dgvItemEdit.CurrentCell.RowIndex].Value.ToString();
        }
    }
}
