
If Exists(Select * from sysobjects where name='prc_CategorySave' and xtype='P')
   Drop Procedure prc_CategorySave

GO
   
Create Procedure prc_CategorySave			
(
	@category_id	varchar(100),
	@category_name	varchar(100),
	@remarks		text
	,@status_		varchar(10)
	,@generated_item_id varchar(100)	= '0' 
	
)
As
	if (@category_id = '0')
	Begin
		Insert into category
		(
			category_id
			,category_name
			,status_ 
			,remarks
			,created_at		-- when this category was created -> will never get updated
			,updated_at		-- only this will get updated
		)
		Values
		(
			@generated_item_id, --sorry my bad guess thsi is fine then   yes it is
			@category_name,
			@status_
			,@remarks
			,GETDATE() 
			,GETDATE() 
		)
	End
	Else
	Begin
		Update category
			Set 
				category_name		= @category_name
				,status_			= @status_
				,remarks			= @remarks
				,updated_at			= getdate()
		Where category_id	= @category_id
  End
GO

-- you can uncomment the line below to ensure the query to insert/save works

 --prc_CategorySave					'0', 'ForkLift Parts', 'Part of forklifts', 'enabled', '1468853643IBW'