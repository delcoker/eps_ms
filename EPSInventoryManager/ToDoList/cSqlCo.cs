using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;
using System.Diagnostics;
using System.Security.AccessControl;
using System.Security.Principal;
using Microsoft.VisualBasic;

namespace EPSInventoryManager				// DAVE change this namespace to your project name space
{


    public class SqlCo
    {

        public static string myUID = "1234567890DEL";
        public static string LICENSE_PATH = "license.del";
        public static string TRANSACTION_POINT_PATH = "transactionPoint.del";

        private SqlCommand cmd;
        private SqlParameter param;
        private SqlConnection con;
        private static string mServerName;


        public static string ServerMySQL;
        public static string PortMySQL;
        public static string UserNameMySQL;
        public static string PwdMySQL;
        public static string DBNameMySQL;
        public static string PassG;
        public static string DropDownLoginFormType;
        
        public static int salter = 13;
        public static string mConstr = "Data Source=.;Initial Catalog=EPS_Project;Integrated Security=True"; // this is not normally how i do it.. nut just to get this running
        // it's normally dynamice so i change from pc to pc
        
        public static string picsPath = @AppDomain.CurrentDomain.BaseDirectory + "pics";

        public SqlCo()
        {
            cmd = new SqlCommand();
            con = new SqlConnection();

            //try
            //{
            //    // http://stackoverflow.com/questions/9108399/how-to-grant-full-permission-to-a-file-created-by-my-application-for-all-users
            //    DirectoryInfo dInfo = new DirectoryInfo(picsPath);
            //    DirectorySecurity dSecurity = dInfo.GetAccessControl();
            //    dSecurity.AddAccessRule(new FileSystemAccessRule(new SecurityIdentifier(WellKnownSidType.WorldSid, null), FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.NoPropagateInherit, AccessControlType.Allow));
            //    dInfo.SetAccessControl(dSecurity);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //}
        }

        public static string GetCodeHashed()
        {
            string path = LICENSE_PATH;

            if (!File.Exists(path))
            {
                AllocConsole();

                //File.Create(path);
                TextWriter tw = new StreamWriter(path);
                Console.WriteLine("Please enter your license carefully\n");
                tw.WriteLine(Console.ReadLine());
                tw.Close();
            }
            string cod = System.IO.File.ReadAllText(LICENSE_PATH).Trim();
            //              string rand = 

            return cod;
        }

        public static string ReEnterCodeHashed()
        {
            string path = LICENSE_PATH;

            //            if (!File.Exists(path))
            //            {
            AllocConsole();

            //File.Create(path);
            TextWriter tw = new StreamWriter(path);
            Console.WriteLine("Please enter your license carefully\n");
            tw.WriteLine(Console.ReadLine());
            tw.Close();
            //            }
            string cod = System.IO.File.ReadAllText(LICENSE_PATH).Trim();
            //            string rand = 

            return cod;
        }

        public void CheckDatabase()
        {
            //            return;
            if (!CheckDatabaseExists())
            {
                //                GenerateDatabase();
            }
        }

        private bool CheckDatabaseExists()
        {
            con = new SqlConnection();
            return this.openConnection();
        }

        public static bool UseComboBox()
        {
            string path = "combobox.del";

            if (!File.Exists(path))
            {
                AllocConsole();

                //File.Create(path);
                TextWriter tw = new StreamWriter(path);
                Console.WriteLine("Please select whether to use a combo box at the login screen\ny for yes.\nn for no.");
                tw.WriteLine(Console.ReadLine());
                tw.Close();
            }

            string yesOrNo = System.IO.File.ReadAllText(@"combobox.del");

            if (yesOrNo.Contains("y"))
            {
                return true;
            }
            return false;
        }

        public static void UseComboBox_2()
        {
            string path = "combobox.del";
            AllocConsole();

            do
            {
                TextWriter tw = new StreamWriter(path);
                Console.WriteLine("Please select whether to use a combo box at the login screen\ny for yes.\nn for no.");
                tw.WriteLine(Console.ReadLine());
                tw.Close();
            } while (!(System.IO.File.ReadAllText(@path).Trim().Contains("n") || System.IO.File.ReadAllText(@path).Trim().Contains("y")));


            // Restart the application
            restartApp();
        }

        // method to change company
      
        //public static bool checkTransactionPointExists(string institutionId, string vendorId)
        //{
        //    //            cTransactionPoint tp = null;

        //    // if file doesn't exist
        //    if (!File.Exists(TRANSACTION_POINT_PATH))
        //    {
        //        // return if it was able to create the transaction point
        //        return (createTransactionPoint(institutionId, vendorId));
        //    }
        //    return true;
        //}

        //public static bool createTransactionPoint(string institutionId, string vendorId)
        //{
        //    //////////// have to clarify with Philip

        //    if (!File.Exists(TRANSACTION_POINT_PATH))
        //    {
        //        AllocConsole();

        //        //File.Create(path);
        //        TextWriter tw = new StreamWriter(TRANSACTION_POINT_PATH);
        //        Console.WriteLine("Please name this transaction point\n");
        //        tw.WriteLine(Console.ReadLine());
        //        tw.Close();
        //    }
        //    string tp = System.IO.File.ReadAllText(TRANSACTION_POINT_PATH).Trim();


        //    cTransactionPoint n = new cTransactionPoint("0", tp, vendorId, institutionId, "enabled", true);
        //    string tpID = n.TransactionPointSave();
        //    if (tpID != "-1")
        //    {
        //        // save transaction point id
        //        TextWriter tw = new StreamWriter(TRANSACTION_POINT_PATH);
        //        tw.WriteLine(tpID);
        //        tw.Close();

        //        //                n.TransactionPointID();
        //        ////////


        //        MessageBox.Show("New transaction point made");
        //        return true;
        //    }

        //    ////////// if philip supplies me with transaction points... comment above out supplies transaction points (name, vendor_id, institution_id, status)

        //    if (!File.Exists(TRANSACTION_POINT_PATH))
        //    {
        //        //vTransactionPointrSelect tps = new vTransactionPointSelect(vendorId, institutionId);
        //        //tps.ShowDialog();
        //        //return vTransactionPointSelect.CREATED_TRANSACTION_POINT;
        //    }
        //    return false;
        //}

        public static bool setTransactionPoint(string transaction_point_id)
        {

            if (!File.Exists(TRANSACTION_POINT_PATH))
            {
                TextWriter tw = new StreamWriter(TRANSACTION_POINT_PATH);
                tw.WriteLine(transaction_point_id);
                tw.Close();
            }

            string tranPoint = System.IO.File.ReadAllText(TRANSACTION_POINT_PATH);

            if (tranPoint.Length > 3)
            {
                return true;
            }
            return false;
        }

        public static string getTransactionPoint()
        {
            return System.IO.File.ReadAllText(TRANSACTION_POINT_PATH);
        }



        private static void restartApp()
        {
            ProcessStartInfo Info = new ProcessStartInfo();
            Info.Arguments = "/C ping 127.0.0.1 -n 2 && \"" + Application.ExecutablePath + "\"";
            Info.WindowStyle = ProcessWindowStyle.Hidden;
            Info.CreateNoWindow = true;
            Info.FileName = "cmd.exe";
            Process.Start(Info);
            Application.Exit();
        }

        public static string GetCompanyName()
        {
            string path = "company.del";

            if (!File.Exists(path))
            {
                AllocConsole();

                //File.Create(path);
                TextWriter tw = new StreamWriter(path);
                Console.WriteLine("Please enter your company carefully\n ");
                tw.WriteLine(Console.ReadLine());
                tw.Close();
            }

            return System.IO.File.ReadAllText(@"company.del").Trim();
        }

        public static string passWordChange()
        {
            AllocConsole();
            Console.Write("Please hit enter and type in your old password carefully\n");
            Console.WriteLine("net use password *", "net use password *");
            string[] args = { "net", "use", "password", "*" };
            Console.WriteLine("Number of command line parameters = {0}", args.Length);
            for (int i = 0; i < args.Length; i++)
            {
                Console.WriteLine("Arg[{0}] = [{1}]", i, args[i]);
            }

            var button = Console.ReadKey();

            string old = Console.ReadLine();

            return old;
        }

        public static void saveLoginTimes()
        {
            string AppName = Application.ProductName;
            Interaction.SaveSetting(AppName, "DBSection", "DB_Port", PortMySQL);
        }

        public static void SaveData()
        {
            string AppName = Application.ProductName;

            Interaction.SaveSetting(AppName, "DBSection", "DB_Name", DBNameMySQL);
            Interaction.SaveSetting(AppName, "DBSection", "DB_IP", ServerMySQL);
            Interaction.SaveSetting(AppName, "DBSection", "DB_Port", PortMySQL);
            Interaction.SaveSetting(AppName, "DBSection", "DB_User", UserNameMySQL);
            Interaction.SaveSetting(AppName, "DBSection", "DB_Password", PwdMySQL);
            Interaction.SaveSetting(AppName, "DBSection", "DropDownLoginFormType", DropDownLoginFormType);

            //Interaction.SaveSetting(AppName, "DBSection", "G_Password", PassG);

            Interaction.MsgBox("Database connection settings are saved.", MsgBoxStyle.Information);
        }

        public void getData()
        {
            string AppName = Application.ProductName;

            try
            {
                // first time connect
                DBNameMySQL = Interaction.GetSetting(AppName, "DBSection", "DB_Name", "_pos_db");
                ServerMySQL = Interaction.GetSetting(AppName, "DBSection", "DB_IP", "\\sqlexpress");
                PortMySQL = Interaction.GetSetting(AppName, "DBSection", "DB_Port", "6.21.3.11");
                UserNameMySQL = Interaction.GetSetting(AppName, "DBSection", "DB_User", "_pos_admin");
                PwdMySQL = Interaction.GetSetting(AppName, "DBSection", "DB_Password", "123456");
                //PassG = Interaction.GetSetting(AppName, "DBSection", "G_Password", "PassG");

                DropDownLoginFormType = Interaction.GetSetting(AppName, "DBSection", "DropDownLoginFormType", "DropDownLoginFormType");

                //                try to connect
                Constr = "Server=" + ServerMySQL + "Database =" + DBNameMySQL + "; Integrated Security=False; User Id=" + UserNameMySQL + "; Password=" + PwdMySQL + ";";
                //testCon();


            }
            catch
            {
                Interaction.MsgBox("System registry was not established, you must set/save " + "these settings ", MsgBoxStyle.Information);
                //vNDBConfiguration n = new vNDBConfiguration();
                //n.ShowDialog();

            }

        }

        public bool testCon()
        {
            con.ConnectionString = Constr;
            cmd.Connection = con;

            con.Open();
            cmd.CommandType = CommandType.StoredProcedure;

            return true;

        }

        public bool openConnection()
        {
            string path = "del.del";

            // to enter db server in cmd

            if (!File.Exists(path))
            {
                AllocConsole();

                //File.Create(path);
                TextWriter tw = new StreamWriter(path);
                Console.WriteLine("Please key in server name carefully;\n");
                tw.WriteLine(Console.ReadLine());
                tw.Close();
            }

            con.ConnectionString = Constr;
            cmd.Connection = con;
            try
            {
                con.Open();
                cmd.CommandType = CommandType.StoredProcedure;

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + "\nCould not connect to server.\nContact deLoop 050 212 8010");

                return false;
            }
        }

        private void GenerateDatabase()
        {
            List<string> cmds = new List<string>();
            if (File.Exists(Application.StartupPath + "\\sos_dump.sql"))
            {
                TextReader tr = new StreamReader(Application.StartupPath + "\\sos_dump.sql");
                string line = "";
                string cmd = "";
                while ((line = tr.ReadLine()) != null)
                {
                    if (line.Trim().ToUpper() == "GO")
                    {
                        cmds.Add(cmd);
                        cmd = "";
                    }
                    else
                    {
                        cmd += line + "\r\n";
                    }

                }
                if (cmd.Length > 0)
                {
                    cmds.Add(cmd);
                    cmd = "";
                }
                tr.Close();
            }
            if (cmds.Count > 0)
            {
                SqlCommand command = new SqlCommand();
                // Sql Connection for Master Database
                command.Connection = new SqlConnection(mConstr);
                command.CommandType = System.Data.CommandType.Text;
                command.Connection.Open();
                for (int i = 0; i < cmds.Count; i++)
                {
                    command.CommandText = cmds[i];
                    command.ExecuteNonQuery();
                }
            }
        }
        
        public void query(string parameterName, string parameterValue)
        {
            param = new SqlParameter();
            param.ParameterName = parameterName;
            param.SqlDbType = SqlDbType.VarChar;
            param.Direction = ParameterDirection.Input;
            param.Value = parameterValue;
            cmd.Parameters.Add(param);
        }
        public void query(string parameterName, int parameterValue)
        {
            param = new SqlParameter();
            param.ParameterName = parameterName;
            param.SqlDbType = SqlDbType.Int;
            param.Direction = ParameterDirection.Input;
            param.Value = parameterValue;
            cmd.Parameters.Add(param);
        }
        public void query(string parameterName, decimal parameterValue)
        {
            param = new SqlParameter();
            param.ParameterName = parameterName;
            param.SqlDbType = SqlDbType.Decimal;
            param.Direction = ParameterDirection.Input;
            param.Value = parameterValue;
            cmd.Parameters.Add(param);
        }
        public void query(string parameterName, bool parameterValue)
        {
            param = new SqlParameter();
            param.ParameterName = parameterName;
            param.SqlDbType = SqlDbType.Bit;
            param.Direction = ParameterDirection.Input;
            param.Value = parameterValue;
            cmd.Parameters.Add(param);
        }

             

        // load status (enabled, disabled, deleted) into combobox
        public static void loadStatus(ComboBox cbx)
        {
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            comboSource.Add("enabled", "Enabled");
            comboSource.Add("disabled", "Disabled");
            comboSource.Add("deleted", "Deleted");

            cbx.DataSource = new BindingSource(comboSource, null);
            cbx.DisplayMember = "value";
            cbx.ValueMember = "key";

            cbx.SelectedIndex = cbx.FindStringExact("enabled");
        }

        //load location ("Tema, Tarkwa") warehouse locations
        public static void loadLocation(ComboBox cbx) //works with combo box controls only as parameters
            {
            Dictionary<string, string> comboSource = new Dictionary<string, string>();
            comboSource.Add("tema", "Tema");
            comboSource.Add("tarkwa", "Tarkwa");

            cbx.DataSource = new BindingSource(comboSource, null);
            cbx.DisplayMember = "value";
            cbx.ValueMember = "key";

            cbx.SelectedIndex = cbx.FindStringExact("Tema");
            }
 

        public static string CalculateSHA1(string text, Encoding enc)
        {
            byte[] buffer = enc.GetBytes(text);
            SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
            return BitConverter.ToString(cryptoTransformSHA1.ComputeHash(buffer)).Replace("-", "");
        }

        public static string CalculateSHA1UTF8(string text)
        {
            byte[] buffer = Encoding.UTF8.GetBytes(text);
            SHA1CryptoServiceProvider cryptoTransformSHA1 = new SHA1CryptoServiceProvider();
            return BitConverter.ToString(cryptoTransformSHA1.ComputeHash(buffer)).Replace("-", "");
        }

        //https://msdn.microsoft.com/en-us/library/ezwyzy7b.aspx
        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AllocConsole();

        public static double ConvertToUnixTimestamp(DateTime date)
        {
            DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            TimeSpan diff = date.ToUniversalTime() - origin;
            return Math.Floor(diff.TotalSeconds);
        }

        public static string RandomStringGenerate()
        {
            double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
            return epoch + SqlCo.RandomString(3);
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "delcoker";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public SqlCommand Cmd
        {
            get { return cmd; }
            set { cmd = value; }
        }

        public SqlParameter Param
        {
            get { return param; }
            set { param = value; }
        }

        public SqlConnection Con
        {
            get { return con; }
            set { con = value; }
        }

        public string Constr
        {
            get { return mConstr; }
            set { mConstr = value; }
        }


        public static string MyUID
        {
            get { return myUID; }
            set { myUID = value; }
        }

        //public static void dateNibbies(ToolStripStatusLabel ToolStripStatusLabelDate, ToolStripStatusLabel ToolStripStatusLabelUser, Label lblLogger, cUser LoggedUser, ToolStripStatusLabel company)
        //{
        //    ToolStripStatusLabelDate.Text = DateTime.Now.ToShortDateString();
        //    ToolStripStatusLabelUser.Text = LoggedUser.FirstName + " " + LoggedUser.LastName;
        //    lblLogger.Text = ToolStripStatusLabelUser.Text;
        //    company.Text = LoggedUser.Vend.Name;
        //}

        public void DisconnMy()
        {
            Con.Close();
            Con.Dispose();
        }
    }
}
