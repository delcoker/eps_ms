
If Exists(Select * from sysobjects where name='prc_ItemsGet' and xtype='P')
   Drop Procedure prc_ItemsGet

GO

Create Procedure prc_ItemsGet  -- 0
(
   @item_id varchar(100) = '0'
)
As

    If (@item_id = '0')
    
		Select  item_id,
				item_name,
				part_number,
				item_manufacture_date,
				item_expiry_date,
				item_location,
				remarks,
				created_at,
				updated_at

		From item
		Order by item_name Asc

	Else
		Select 
				item_id,
				item_name,
				part_number,
				item_manufacture_date,
				item_expiry_date,
				item_location,
				remarks,
				created_at,
				updated_at
		From item
		where item_id = @item_id
		Order by item_name Asc


	--	,created_at = Convert(varchar(12), created_at,106)