create table category
(
	category_id		varchar(100) primary key,
	category_name	varchar(100),
	remarks		text,
	status_			varchar(10) check(status_ in ('enabled', 'disabled', 'deleted')) default 'enabled',
	created_at		smalldatetime,
	updated_at		smalldatetime
)
