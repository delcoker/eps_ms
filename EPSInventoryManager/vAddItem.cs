﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Data.SqlClient;


namespace EPSInventoryManager
{
    public partial class frmAddItem : MetroForm
    {

        public frmAddItem()
        {
            InitializeComponent();
        }


        public void loadComboBoxes()
        {
            //SqlCo.loadLocation(cbxItemLocation);
            SqlCo.loadStatus(cbxAddItemStatus);

            // now to load data from the database into the combo box --ok
            // You need your dataset object
            DataSet ds = new DataSet();

            // you category object
            cItemCategory cat = new cItemCategory();
            // from the stored procedure, to get all item when fetching the category id should be 0
            cat.CategoryId = "0";
            // now just call the function to fetch data and store what it gets in the dataset
            ds = cat.CategoryGet();

            // now bind the data to the combo box --ok
            cbxItemCategory.DataSource = ds.Tables[0].DefaultView;
            cbxItemCategory.ValueMember = "category_id";
            cbxItemCategory.DisplayMember = "category_name";
        }

        private void btnCancelAddItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddItem_Click(object sender, EventArgs e)//save record to database
        {
            if (txtAddItemName.Text.Equals(String.Empty)) //txtAddItemName.text =""
            {
                MessageBox.Show("Please enter an item name");
                return;
            }


            cItem it = new cItem("0", txtAddItemName.Text, Convert.ToDecimal(numPrice.Value), txtUnitMes.Text, cbxItemCategory.SelectedValue.ToString(), Convert.ToInt32(nupMinStockLevel.Value),
            cbxAddItemStatus.SelectedValue.ToString(), txtItemDescription.Text, true);


            if (!it.ItemSave())
            {
                MessageBox.Show("Could not save");
            }
            else
            {
                MessageBox.Show("Saved");
            }
        }


        private void frmAddItem_Load(object sender, EventArgs e)
        {
            loadComboBoxes();
        }

        

 
    }

}