﻿namespace EPSInventoryManager
{
    partial class vItemEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dgvItemEdit = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnAddItem = new MetroFramework.Controls.MetroButton();
            this.panel6 = new System.Windows.Forms.Panel();
            this.cbxAddItemStatus = new MetroFramework.Controls.MetroComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.cbxItemCategory = new MetroFramework.Controls.MetroComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtAddItemName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.numPrice = new System.Windows.Forms.NumericUpDown();
            this.nupMinStockLevel = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtItemDescription = new MetroFramework.Controls.MetroTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.txtUnitMes = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemEdit)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupMinStockLevel)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 90.63042F));
            this.tableLayoutPanel1.Controls.Add(this.dgvItemEdit, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.53877F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.46123F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(898, 673);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dgvItemEdit
            // 
            this.dgvItemEdit.AllowUserToAddRows = false;
            this.dgvItemEdit.AllowUserToDeleteRows = false;
            this.dgvItemEdit.AllowUserToOrderColumns = true;
            this.dgvItemEdit.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvItemEdit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvItemEdit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvItemEdit.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvItemEdit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvItemEdit.Location = new System.Drawing.Point(2, 296);
            this.dgvItemEdit.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dgvItemEdit.Name = "dgvItemEdit";
            this.dgvItemEdit.ReadOnly = true;
            this.dgvItemEdit.RowTemplate.Height = 24;
            this.dgvItemEdit.Size = new System.Drawing.Size(894, 374);
            this.dgvItemEdit.TabIndex = 85;
            this.dgvItemEdit.TabStop = false;
            this.dgvItemEdit.Click += new System.EventHandler(this.dgvItemEdit_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.32662F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.67338F));
            this.tableLayoutPanel3.Controls.Add(this.panel8, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.panel6, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel4, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel5, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.609F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.03114F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.35986F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(894, 289);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnAddItem);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel8.Location = new System.Drawing.Point(290, 104);
            this.panel8.Margin = new System.Windows.Forms.Padding(2);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(602, 183);
            this.panel8.TabIndex = 1;
            // 
            // btnAddItem
            // 
            this.btnAddItem.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAddItem.Location = new System.Drawing.Point(3, 0);
            this.btnAddItem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAddItem.Name = "btnAddItem";
            this.btnAddItem.Size = new System.Drawing.Size(106, 36);
            this.btnAddItem.TabIndex = 6;
            this.btnAddItem.Text = "&Save";
            this.btnAddItem.UseSelectable = true;
            this.btnAddItem.Click += new System.EventHandler(this.btnAddItem_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.cbxAddItemStatus);
            this.panel6.Controls.Add(this.label9);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(290, 50);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(602, 50);
            this.panel6.TabIndex = 1;
            // 
            // cbxAddItemStatus
            // 
            this.cbxAddItemStatus.FormattingEnabled = true;
            this.cbxAddItemStatus.ItemHeight = 23;
            this.cbxAddItemStatus.Location = new System.Drawing.Point(0, 13);
            this.cbxAddItemStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxAddItemStatus.Name = "cbxAddItemStatus";
            this.cbxAddItemStatus.Size = new System.Drawing.Size(264, 29);
            this.cbxAddItemStatus.TabIndex = 5;
            this.cbxAddItemStatus.UseSelectable = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 81;
            this.label9.Text = "Status:";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.cbxItemCategory);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(290, 2);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(602, 44);
            this.panel4.TabIndex = 1;
            // 
            // cbxItemCategory
            // 
            this.cbxItemCategory.FormattingEnabled = true;
            this.cbxItemCategory.ItemHeight = 23;
            this.cbxItemCategory.Location = new System.Drawing.Point(0, 13);
            this.cbxItemCategory.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxItemCategory.Name = "cbxItemCategory";
            this.cbxItemCategory.Size = new System.Drawing.Size(264, 29);
            this.cbxItemCategory.TabIndex = 4;
            this.cbxItemCategory.UseSelectable = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 80;
            this.label6.Text = "Category:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtAddItemName);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Location = new System.Drawing.Point(2, 2);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(283, 44);
            this.panel3.TabIndex = 0;
            // 
            // txtAddItemName
            // 
            this.txtAddItemName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAddItemName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAddItemName.Location = new System.Drawing.Point(0, 13);
            this.txtAddItemName.Name = "txtAddItemName";
            this.txtAddItemName.Size = new System.Drawing.Size(283, 20);
            this.txtAddItemName.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 72;
            this.label1.Text = "Name:";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.label12);
            this.panel5.Controls.Add(this.numPrice);
            this.panel5.Controls.Add(this.nupMinStockLevel);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(2, 50);
            this.panel5.Margin = new System.Windows.Forms.Padding(2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(284, 50);
            this.panel5.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(146, 0);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 84;
            this.label12.Text = "Price:";
            // 
            // numPrice
            // 
            this.numPrice.Location = new System.Drawing.Point(149, 16);
            this.numPrice.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.numPrice.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPrice.Name = "numPrice";
            this.numPrice.Size = new System.Drawing.Size(132, 20);
            this.numPrice.TabIndex = 85;
            // 
            // nupMinStockLevel
            // 
            this.nupMinStockLevel.Location = new System.Drawing.Point(2, 16);
            this.nupMinStockLevel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.nupMinStockLevel.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.nupMinStockLevel.Name = "nupMinStockLevel";
            this.nupMinStockLevel.Size = new System.Drawing.Size(133, 20);
            this.nupMinStockLevel.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Location = new System.Drawing.Point(0, 0);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 13);
            this.label4.TabIndex = 79;
            this.label4.Text = "Min. Stock Level:";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.panel9, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel7, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(2, 104);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.59016F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75.40984F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(284, 183);
            this.tableLayoutPanel4.TabIndex = 80;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtItemDescription);
            this.panel9.Controls.Add(this.label2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(2, 46);
            this.panel9.Margin = new System.Windows.Forms.Padding(2);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(280, 135);
            this.panel9.TabIndex = 79;
            // 
            // txtItemDescription
            // 
            // 
            // 
            // 
            this.txtItemDescription.CustomButton.Image = null;
            this.txtItemDescription.CustomButton.Location = new System.Drawing.Point(160, 2);
            this.txtItemDescription.CustomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtItemDescription.CustomButton.Name = "";
            this.txtItemDescription.CustomButton.Size = new System.Drawing.Size(117, 117);
            this.txtItemDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtItemDescription.CustomButton.TabIndex = 1;
            this.txtItemDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtItemDescription.CustomButton.UseSelectable = true;
            this.txtItemDescription.CustomButton.Visible = false;
            this.txtItemDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtItemDescription.Lines = new string[0];
            this.txtItemDescription.Location = new System.Drawing.Point(0, 13);
            this.txtItemDescription.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtItemDescription.MaxLength = 32767;
            this.txtItemDescription.Multiline = true;
            this.txtItemDescription.Name = "txtItemDescription";
            this.txtItemDescription.PasswordChar = '\0';
            this.txtItemDescription.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtItemDescription.SelectedText = "";
            this.txtItemDescription.SelectionLength = 0;
            this.txtItemDescription.SelectionStart = 0;
            this.txtItemDescription.ShortcutsEnabled = true;
            this.txtItemDescription.Size = new System.Drawing.Size(280, 122);
            this.txtItemDescription.TabIndex = 3;
            this.txtItemDescription.UseSelectable = true;
            this.txtItemDescription.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtItemDescription.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 13);
            this.label2.TabIndex = 78;
            this.label2.Text = "Long Description:";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.txtUnitMes);
            this.panel7.Controls.Add(this.label5);
            this.panel7.Location = new System.Drawing.Point(2, 2);
            this.panel7.Margin = new System.Windows.Forms.Padding(2);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(279, 40);
            this.panel7.TabIndex = 78;
            // 
            // txtUnitMes
            // 
            this.txtUnitMes.Location = new System.Drawing.Point(0, 13);
            this.txtUnitMes.Name = "txtUnitMes";
            this.txtUnitMes.Size = new System.Drawing.Size(133, 20);
            this.txtUnitMes.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Location = new System.Drawing.Point(0, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 89;
            this.label5.Text = "Unit of Measure:";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(10, 69);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(898, 673);
            this.panel1.TabIndex = 1;
            // 
            // vItemEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 750);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(920, 750);
            this.MinimumSize = new System.Drawing.Size(920, 750);
            this.Name = "vItemEdit";
            this.Padding = new System.Windows.Forms.Padding(10, 60, 10, 10);
            this.Text = "Item Edit";
            this.Load += new System.EventHandler(this.vItemEdit_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvItemEdit)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nupMinStockLevel)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.NumericUpDown nupMinStockLevel;
        private MetroFramework.Controls.MetroTextBox txtItemDescription;
        private System.Windows.Forms.Label label9;
        private MetroFramework.Controls.MetroComboBox cbxAddItemStatus;
        private System.Windows.Forms.Label label6;
        private MetroFramework.Controls.MetroComboBox cbxItemCategory;
        private MetroFramework.Controls.MetroButton btnAddItem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgvItemEdit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox txtUnitMes;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAddItemName;
        private System.Windows.Forms.NumericUpDown numPrice;
        private System.Windows.Forms.Label label12;
    }
}