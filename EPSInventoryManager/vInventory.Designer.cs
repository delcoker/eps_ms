﻿namespace EPSInventoryManager
    {
    partial class frmInventory
        {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
            {
            if (disposing && (components != null))
                {
                components.Dispose();
                }
            base.Dispose(disposing);
            }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
            {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.metrodgvItem = new MetroFramework.Controls.MetroGrid();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtWayBil = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.numQtyInvAdd = new System.Windows.Forms.NumericUpDown();
            this.txtPart = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.chkExpiry = new System.Windows.Forms.CheckBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbxItems = new MetroFramework.Controls.MetroComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbxInvCategory = new MetroFramework.Controls.MetroComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.cbxInvStatus = new MetroFramework.Controls.MetroComboBox();
            this.numPrice = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDescription = new MetroFramework.Controls.MetroTextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.numMinStockLevel = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnTopUp = new MetroFramework.Controls.MetroButton();
            this.btnCancel = new MetroFramework.Controls.MetroButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label29 = new System.Windows.Forms.Label();
            this.cbxPurchaseType = new MetroFramework.Controls.MetroComboBox();
            this.cbxInvLocation = new MetroFramework.Controls.MetroComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.cbxTransactionType = new MetroFramework.Controls.MetroComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.cbxBusinessPartner = new MetroFramework.Controls.MetroComboBox();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.metroTextBox8 = new MetroFramework.Controls.MetroTextBox();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.metrodgvItem)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtyInvAdd)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinStockLevel)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel11, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(15, 60);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 2;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 354F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1252, 643);
            this.tableLayoutPanel9.TabIndex = 61;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Controls.Add(this.metrodgvItem, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 357);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1246, 283);
            this.tableLayoutPanel10.TabIndex = 1;
            // 
            // metrodgvItem
            // 
            this.metrodgvItem.AllowUserToAddRows = false;
            this.metrodgvItem.AllowUserToDeleteRows = false;
            this.metrodgvItem.AllowUserToOrderColumns = true;
            this.metrodgvItem.AllowUserToResizeRows = false;
            this.metrodgvItem.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.metrodgvItem.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metrodgvItem.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.metrodgvItem.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.metrodgvItem.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metrodgvItem.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.metrodgvItem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.metrodgvItem.DefaultCellStyle = dataGridViewCellStyle11;
            this.metrodgvItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metrodgvItem.EnableHeadersVisualStyles = false;
            this.metrodgvItem.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            this.metrodgvItem.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.metrodgvItem.Location = new System.Drawing.Point(2, 3);
            this.metrodgvItem.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.metrodgvItem.Name = "metrodgvItem";
            this.metrodgvItem.ReadOnly = true;
            this.metrodgvItem.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(174)))), ((int)(((byte)(219)))));
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Segoe UI", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Pixel);
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(198)))), ((int)(((byte)(247)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(17)))), ((int)(((byte)(17)))), ((int)(((byte)(17)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.metrodgvItem.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.metrodgvItem.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.metrodgvItem.RowTemplate.Height = 24;
            this.metrodgvItem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.metrodgvItem.Size = new System.Drawing.Size(1242, 277);
            this.metrodgvItem.Style = MetroFramework.MetroColorStyle.Blue;
            this.metrodgvItem.TabIndex = 507;
            this.metrodgvItem.TabStop = false;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.AutoSize = true;
            this.tableLayoutPanel11.ColumnCount = 4;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel11.Controls.Add(this.groupBox3, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.groupBox4, 3, 0);
            this.tableLayoutPanel11.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1246, 348);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtWayBil);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.numQtyInvAdd);
            this.groupBox3.Controls.Add(this.txtPart);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.chkExpiry);
            this.groupBox3.Controls.Add(this.dateTimePicker1);
            this.groupBox3.Controls.Add(this.dateTimePicker2);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(625, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(305, 342);
            this.groupBox3.TabIndex = 508;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Stock details";
            // 
            // txtWayBil
            // 
            this.txtWayBil.Location = new System.Drawing.Point(17, 47);
            this.txtWayBil.Name = "txtWayBil";
            this.txtWayBil.Size = new System.Drawing.Size(275, 20);
            this.txtWayBil.TabIndex = 0;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(14, 31);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(84, 13);
            this.label17.TabIndex = 104;
            this.label17.Text = "Waybill Number:";
            // 
            // numQtyInvAdd
            // 
            this.numQtyInvAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numQtyInvAdd.Location = new System.Drawing.Point(163, 246);
            this.numQtyInvAdd.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.numQtyInvAdd.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numQtyInvAdd.Name = "numQtyInvAdd";
            this.numQtyInvAdd.Size = new System.Drawing.Size(129, 26);
            this.numQtyInvAdd.TabIndex = 3;
            // 
            // txtPart
            // 
            this.txtPart.Location = new System.Drawing.Point(17, 86);
            this.txtPart.Name = "txtPart";
            this.txtPart.Size = new System.Drawing.Size(275, 20);
            this.txtPart.TabIndex = 1;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(165, 230);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(82, 13);
            this.label19.TabIndex = 101;
            this.label19.Text = "Quantity to add:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(14, 70);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(69, 13);
            this.label20.TabIndex = 102;
            this.label20.Text = "Part Number:";
            // 
            // chkExpiry
            // 
            this.chkExpiry.AutoSize = true;
            this.chkExpiry.Location = new System.Drawing.Point(16, 112);
            this.chkExpiry.Name = "chkExpiry";
            this.chkExpiry.Size = new System.Drawing.Size(82, 17);
            this.chkExpiry.TabIndex = 2;
            this.chkExpiry.Text = "Has Expiry?";
            this.chkExpiry.UseVisualStyleBackColor = true;
            this.chkExpiry.CheckedChanged += new System.EventHandler(this.chkExpiry_CheckStateChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Enabled = false;
            this.dateTimePicker1.Location = new System.Drawing.Point(17, 147);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(275, 20);
            this.dateTimePicker1.TabIndex = 3;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Enabled = false;
            this.dateTimePicker2.Location = new System.Drawing.Point(17, 192);
            this.dateTimePicker2.Margin = new System.Windows.Forms.Padding(2);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(275, 20);
            this.dateTimePicker2.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(17, 177);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(64, 13);
            this.label21.TabIndex = 96;
            this.label21.Text = "Expiry Date:";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(17, 131);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(107, 13);
            this.label22.TabIndex = 98;
            this.label22.Text = "Date Of Manufacture";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbxItems);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.cbxInvCategory);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.cbxInvStatus);
            this.groupBox1.Controls.Add(this.numPrice);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.txtDescription);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.numMinStockLevel);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 342);
            this.groupBox1.TabIndex = 506;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Item details";
            // 
            // cbxItems
            // 
            this.cbxItems.FormattingEnabled = true;
            this.cbxItems.ItemHeight = 23;
            this.cbxItems.Location = new System.Drawing.Point(15, 47);
            this.cbxItems.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxItems.Name = "cbxItems";
            this.cbxItems.Size = new System.Drawing.Size(275, 29);
            this.cbxItems.TabIndex = 0;
            this.cbxItems.UseSelectable = true;
            this.cbxItems.SelectedIndexChanged += new System.EventHandler(this.cbxItems_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(16, 31);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(30, 13);
            this.label23.TabIndex = 111;
            this.label23.Text = "Item:";
            // 
            // cbxInvCategory
            // 
            this.cbxInvCategory.Enabled = false;
            this.cbxInvCategory.FormattingEnabled = true;
            this.cbxInvCategory.ItemHeight = 23;
            this.cbxInvCategory.Location = new System.Drawing.Point(15, 99);
            this.cbxInvCategory.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxInvCategory.Name = "cbxInvCategory";
            this.cbxInvCategory.Size = new System.Drawing.Size(275, 29);
            this.cbxInvCategory.TabIndex = 1;
            this.cbxInvCategory.TabStop = false;
            this.cbxInvCategory.UseSelectable = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(17, 293);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(90, 13);
            this.label24.TabIndex = 114;
            this.label24.Text = "Min. Stock Level:";
            // 
            // cbxInvStatus
            // 
            this.cbxInvStatus.Enabled = false;
            this.cbxInvStatus.FormattingEnabled = true;
            this.cbxInvStatus.ItemHeight = 23;
            this.cbxInvStatus.Location = new System.Drawing.Point(15, 147);
            this.cbxInvStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxInvStatus.Name = "cbxInvStatus";
            this.cbxInvStatus.Size = new System.Drawing.Size(275, 29);
            this.cbxInvStatus.TabIndex = 2;
            this.cbxInvStatus.TabStop = false;
            this.cbxInvStatus.UseSelectable = true;
            // 
            // numPrice
            // 
            this.numPrice.Enabled = false;
            this.numPrice.Location = new System.Drawing.Point(174, 309);
            this.numPrice.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.numPrice.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numPrice.Name = "numPrice";
            this.numPrice.Size = new System.Drawing.Size(116, 20);
            this.numPrice.TabIndex = 8;
            this.numPrice.TabStop = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(171, 293);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(34, 13);
            this.label25.TabIndex = 117;
            this.label25.Text = "Price:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(15, 177);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(63, 13);
            this.label26.TabIndex = 110;
            this.label26.Text = "Description:";
            // 
            // txtDescription
            // 
            // 
            // 
            // 
            this.txtDescription.CustomButton.Image = null;
            this.txtDescription.CustomButton.Location = new System.Drawing.Point(196, 1);
            this.txtDescription.CustomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDescription.CustomButton.Name = "";
            this.txtDescription.CustomButton.Size = new System.Drawing.Size(77, 77);
            this.txtDescription.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtDescription.CustomButton.TabIndex = 1;
            this.txtDescription.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtDescription.CustomButton.UseSelectable = true;
            this.txtDescription.CustomButton.Visible = false;
            this.txtDescription.Enabled = false;
            this.txtDescription.Lines = new string[0];
            this.txtDescription.Location = new System.Drawing.Point(15, 193);
            this.txtDescription.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtDescription.MaxLength = 32767;
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.PasswordChar = '\0';
            this.txtDescription.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtDescription.SelectedText = "";
            this.txtDescription.SelectionLength = 0;
            this.txtDescription.SelectionStart = 0;
            this.txtDescription.ShortcutsEnabled = true;
            this.txtDescription.Size = new System.Drawing.Size(274, 79);
            this.txtDescription.TabIndex = 9;
            this.txtDescription.TabStop = false;
            this.txtDescription.UseSelectable = true;
            this.txtDescription.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtDescription.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(16, 131);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(40, 13);
            this.label27.TabIndex = 112;
            this.label27.Text = "Status:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(16, 83);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(52, 13);
            this.label28.TabIndex = 109;
            this.label28.Text = "Category:";
            // 
            // numMinStockLevel
            // 
            this.numMinStockLevel.Enabled = false;
            this.numMinStockLevel.Location = new System.Drawing.Point(16, 309);
            this.numMinStockLevel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.numMinStockLevel.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numMinStockLevel.Name = "numMinStockLevel";
            this.numMinStockLevel.Size = new System.Drawing.Size(116, 20);
            this.numMinStockLevel.TabIndex = 7;
            this.numMinStockLevel.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnTopUp);
            this.groupBox4.Controls.Add(this.btnCancel);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(936, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(307, 342);
            this.groupBox4.TabIndex = 509;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Actions";
            // 
            // btnTopUp
            // 
            this.btnTopUp.Location = new System.Drawing.Point(14, 216);
            this.btnTopUp.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnTopUp.Name = "btnTopUp";
            this.btnTopUp.Size = new System.Drawing.Size(145, 54);
            this.btnTopUp.TabIndex = 0;
            this.btnTopUp.Text = "&TopUp";
            this.btnTopUp.UseSelectable = true;
            this.btnTopUp.Click += new System.EventHandler(this.btnTopUp_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(14, 275);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(146, 54);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseSelectable = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.cbxPurchaseType);
            this.groupBox2.Controls.Add(this.cbxInvLocation);
            this.groupBox2.Controls.Add(this.label30);
            this.groupBox2.Controls.Add(this.cbxTransactionType);
            this.groupBox2.Controls.Add(this.label31);
            this.groupBox2.Controls.Add(this.cbxBusinessPartner);
            this.groupBox2.Controls.Add(this.label32);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Controls.Add(this.metroTextBox8);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(314, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(305, 342);
            this.groupBox2.TabIndex = 507;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Receipt details";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(15, 79);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(82, 13);
            this.label29.TabIndex = 120;
            this.label29.Text = "Purchase Type:";
            // 
            // cbxPurchaseType
            // 
            this.cbxPurchaseType.FormattingEnabled = true;
            this.cbxPurchaseType.ItemHeight = 23;
            this.cbxPurchaseType.Location = new System.Drawing.Point(15, 95);
            this.cbxPurchaseType.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxPurchaseType.Name = "cbxPurchaseType";
            this.cbxPurchaseType.Size = new System.Drawing.Size(275, 29);
            this.cbxPurchaseType.TabIndex = 0;
            this.cbxPurchaseType.UseSelectable = true;
            // 
            // cbxInvLocation
            // 
            this.cbxInvLocation.FormattingEnabled = true;
            this.cbxInvLocation.ItemHeight = 23;
            this.cbxInvLocation.Location = new System.Drawing.Point(15, 193);
            this.cbxInvLocation.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxInvLocation.Name = "cbxInvLocation";
            this.cbxInvLocation.Size = new System.Drawing.Size(275, 29);
            this.cbxInvLocation.TabIndex = 2;
            this.cbxInvLocation.UseSelectable = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(15, 177);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(112, 13);
            this.label30.TabIndex = 122;
            this.label30.Text = "Location(Warehouse):";
            // 
            // cbxTransactionType
            // 
            this.cbxTransactionType.Enabled = false;
            this.cbxTransactionType.FormattingEnabled = true;
            this.cbxTransactionType.ItemHeight = 23;
            this.cbxTransactionType.Location = new System.Drawing.Point(15, 47);
            this.cbxTransactionType.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxTransactionType.Name = "cbxTransactionType";
            this.cbxTransactionType.Size = new System.Drawing.Size(275, 29);
            this.cbxTransactionType.TabIndex = 0;
            this.cbxTransactionType.TabStop = false;
            this.cbxTransactionType.UseSelectable = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(15, 31);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(93, 13);
            this.label31.TabIndex = 118;
            this.label31.Text = "Transaction Type:";
            // 
            // cbxBusinessPartner
            // 
            this.cbxBusinessPartner.FormattingEnabled = true;
            this.cbxBusinessPartner.ItemHeight = 23;
            this.cbxBusinessPartner.Location = new System.Drawing.Point(15, 143);
            this.cbxBusinessPartner.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxBusinessPartner.Name = "cbxBusinessPartner";
            this.cbxBusinessPartner.Size = new System.Drawing.Size(275, 29);
            this.cbxBusinessPartner.TabIndex = 1;
            this.cbxBusinessPartner.UseSelectable = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(15, 128);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(48, 13);
            this.label32.TabIndex = 115;
            this.label32.Text = "Supplier:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(15, 232);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(122, 13);
            this.label33.TabIndex = 103;
            this.label33.Text = "Remarks on transaction:";
            // 
            // metroTextBox8
            // 
            // 
            // 
            // 
            this.metroTextBox8.CustomButton.Image = null;
            this.metroTextBox8.CustomButton.Location = new System.Drawing.Point(193, 1);
            this.metroTextBox8.CustomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.metroTextBox8.CustomButton.Name = "";
            this.metroTextBox8.CustomButton.Size = new System.Drawing.Size(81, 81);
            this.metroTextBox8.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox8.CustomButton.TabIndex = 1;
            this.metroTextBox8.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox8.CustomButton.UseSelectable = true;
            this.metroTextBox8.CustomButton.Visible = false;
            this.metroTextBox8.Lines = new string[0];
            this.metroTextBox8.Location = new System.Drawing.Point(15, 246);
            this.metroTextBox8.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.metroTextBox8.MaxLength = 32767;
            this.metroTextBox8.Multiline = true;
            this.metroTextBox8.Name = "metroTextBox8";
            this.metroTextBox8.PasswordChar = '\0';
            this.metroTextBox8.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox8.SelectedText = "";
            this.metroTextBox8.SelectionLength = 0;
            this.metroTextBox8.SelectionStart = 0;
            this.metroTextBox8.ShortcutsEnabled = true;
            this.metroTextBox8.Size = new System.Drawing.Size(275, 83);
            this.metroTextBox8.TabIndex = 3;
            this.metroTextBox8.UseSelectable = true;
            this.metroTextBox8.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox8.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // frmInventory
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1282, 719);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "frmInventory";
            this.Padding = new System.Windows.Forms.Padding(15, 60, 15, 16);
            this.Text = "Inventory - [Receive Stock]";
            this.Load += new System.EventHandler(this.frmInventory_Load);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.metrodgvItem)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numQtyInvAdd)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPrice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numMinStockLevel)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

            }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private MetroFramework.Controls.MetroGrid metrodgvItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtWayBil;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.NumericUpDown numQtyInvAdd;
        private System.Windows.Forms.TextBox txtPart;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chkExpiry;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.GroupBox groupBox1;
        private MetroFramework.Controls.MetroComboBox cbxItems;
        private System.Windows.Forms.Label label23;
        private MetroFramework.Controls.MetroComboBox cbxInvCategory;
        private System.Windows.Forms.Label label24;
        private MetroFramework.Controls.MetroComboBox cbxInvStatus;
        private System.Windows.Forms.NumericUpDown numPrice;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private MetroFramework.Controls.MetroTextBox txtDescription;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.NumericUpDown numMinStockLevel;
        private System.Windows.Forms.GroupBox groupBox4;
        private MetroFramework.Controls.MetroButton btnTopUp;
        private MetroFramework.Controls.MetroButton btnCancel;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label29;
        private MetroFramework.Controls.MetroComboBox cbxPurchaseType;
        private MetroFramework.Controls.MetroComboBox cbxInvLocation;
        private System.Windows.Forms.Label label30;
        private MetroFramework.Controls.MetroComboBox cbxTransactionType;
        private System.Windows.Forms.Label label31;
        private MetroFramework.Controls.MetroComboBox cbxBusinessPartner;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private MetroFramework.Controls.MetroTextBox metroTextBox8;
    }
    }

