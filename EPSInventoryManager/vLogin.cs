﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroFramework.Forms;
using System.Windows.Forms;

namespace EPSInventoryManager
{

    public partial class vLogin : MetroForm
    {
        public vLogin()
        {
            InitializeComponent();
        }

        private void vLogin_Load(object sender, EventArgs e)
        {
            txtUserName.Focus();
            
        } 

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (txtUserName.Text.Equals(String.Empty))
            {
                MessageBox.Show("Please enter username.", "Login EPS IMS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUserName.Focus();
                return;
            }
            if (txtPassword.Text.Equals(String.Empty) || txtPassword.Text.Length < 3)
            {
                MessageBox.Show("Please enter a valid password.", "Login EPS IMS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPassword.Focus();
                return;
            }

            cUser usr = new cUser("0", txtUserName.Text, txtPassword.Text, false);
            if (!usr.UserCheck())
            {
                MessageBox.Show("Please enter a valid Username and Password.", "Login EPS IMS", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtUserName.Focus();
                return;
            }

            else
            {
                //MessageBox.Show("Login Successful.", "Login EPS IMS.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                frmDashboard dashboard = new frmDashboard();
                dashboard.Show();

            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Please contact your administrator.", "Message.", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
