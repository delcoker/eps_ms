﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPSInventoryManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            //Application.Run(new frmPartnerDetails());        //you can comment this out when you want to run the project from start to finish --ok
            Application.Run(new frmDashboard());
            //Application.Run(new vLogin());
            //Application.Run(new vReports());
            //Application.Run(new Form1());
            //Application.Run(new vPartnerEdit());
            //Application.Run(new Form2());
        }
    }
}
