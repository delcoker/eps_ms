﻿namespace EPSInventoryManager
{
    partial class vReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.prcStockTransactionDetailReportGetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportStDetailDataSet = new EPSInventoryManager.ReportStDetailDataSet();
            this.prc_StockCountReportGetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportStCountDataSet = new EPSInventoryManager.reportStCountDataSet();
            this.prcStockCountReportGetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.prc_StockTransactionDetailReportGetTableAdapter = new EPSInventoryManager.ReportStDetailDataSetTableAdapters.prc_StockTransactionDetailReportGetTableAdapter();
            this.prc_StockCountReportGetTableAdapter = new EPSInventoryManager.reportStCountDataSetTableAdapters.prc_StockCountReportGetTableAdapter();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.metroPanel3 = new MetroFramework.Controls.MetroPanel();
            this.rptStockTransactionDetail = new Microsoft.Reporting.WinForms.ReportViewer();
            this.rptStockCount = new Microsoft.Reporting.WinForms.ReportViewer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.metroPanel2 = new MetroFramework.Controls.MetroPanel();
            this.btnRunReport = new MetroFramework.Controls.MetroButton();
            this.dtpEndDate = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.dtpStartDate = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroPanel1 = new MetroFramework.Controls.MetroPanel();
            this.lstBoxReportType = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.prcStockTransactionDetailReportGetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportStDetailDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prc_StockCountReportGetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportStCountDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.prcStockCountReportGetBindingSource)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.metroPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.metroPanel2.SuspendLayout();
            this.metroPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // prcStockTransactionDetailReportGetBindingSource
            // 
            this.prcStockTransactionDetailReportGetBindingSource.DataMember = "prc_StockTransactionDetailReportGet";
            this.prcStockTransactionDetailReportGetBindingSource.DataSource = this.reportStDetailDataSet;
            // 
            // reportStDetailDataSet
            // 
            this.reportStDetailDataSet.DataSetName = "ReportStDetailDataSet";
            this.reportStDetailDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // prc_StockCountReportGetBindingSource
            // 
            this.prc_StockCountReportGetBindingSource.DataMember = "prc_StockCountReportGet";
            this.prc_StockCountReportGetBindingSource.DataSource = this.reportStCountDataSet;
            // 
            // reportStCountDataSet
            // 
            this.reportStCountDataSet.DataSetName = "reportStCountDataSet";
            this.reportStCountDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // prcStockCountReportGetBindingSource
            // 
            this.prcStockCountReportGetBindingSource.DataMember = "prc_StockCountReportGet";
            this.prcStockCountReportGetBindingSource.DataSource = this.reportStCountDataSet;
            // 
            // prc_StockTransactionDetailReportGetTableAdapter
            // 
            this.prc_StockTransactionDetailReportGetTableAdapter.ClearBeforeFill = true;
            // 
            // prc_StockCountReportGetTableAdapter
            // 
            this.prc_StockCountReportGetTableAdapter.ClearBeforeFill = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.355F));
            this.tableLayoutPanel1.Controls.Add(this.metroPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(20, 60);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1240, 688);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // metroPanel3
            // 
            this.metroPanel3.Controls.Add(this.rptStockTransactionDetail);
            this.metroPanel3.Controls.Add(this.rptStockCount);
            this.metroPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel3.HorizontalScrollbarBarColor = true;
            this.metroPanel3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel3.HorizontalScrollbarSize = 10;
            this.metroPanel3.Location = new System.Drawing.Point(263, 3);
            this.metroPanel3.Name = "metroPanel3";
            this.metroPanel3.Size = new System.Drawing.Size(974, 682);
            this.metroPanel3.TabIndex = 1;
            this.metroPanel3.VerticalScrollbarBarColor = true;
            this.metroPanel3.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel3.VerticalScrollbarSize = 10;
            // 
            // rptStockTransactionDetail
            // 
            reportDataSource1.Name = "ReportDS";
            reportDataSource1.Value = this.prcStockTransactionDetailReportGetBindingSource;
            this.rptStockTransactionDetail.LocalReport.DataSources.Add(reportDataSource1);
            this.rptStockTransactionDetail.LocalReport.ReportEmbeddedResource = "EPSInventoryManager.stock_transdetail_report.rdlc";
            this.rptStockTransactionDetail.Location = new System.Drawing.Point(0, 273);
            this.rptStockTransactionDetail.Name = "rptStockTransactionDetail";
            this.rptStockTransactionDetail.Size = new System.Drawing.Size(396, 246);
            this.rptStockTransactionDetail.TabIndex = 3;
            // 
            // rptStockCount
            // 
            reportDataSource2.Name = "RptVDataSet";
            reportDataSource2.Value = this.prc_StockCountReportGetBindingSource;
            this.rptStockCount.LocalReport.DataSources.Add(reportDataSource2);
            this.rptStockCount.LocalReport.ReportEmbeddedResource = "EPSInventoryManager.stock_count.rdlc";
            this.rptStockCount.Location = new System.Drawing.Point(3, 21);
            this.rptStockCount.Name = "rptStockCount";
            this.rptStockCount.Size = new System.Drawing.Size(396, 246);
            this.rptStockCount.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.metroPanel2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.metroPanel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.89736F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.10264F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(254, 682);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // metroPanel2
            // 
            this.metroPanel2.Controls.Add(this.btnRunReport);
            this.metroPanel2.Controls.Add(this.dtpEndDate);
            this.metroPanel2.Controls.Add(this.metroLabel2);
            this.metroPanel2.Controls.Add(this.dtpStartDate);
            this.metroPanel2.Controls.Add(this.metroLabel1);
            this.metroPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel2.HorizontalScrollbarBarColor = true;
            this.metroPanel2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel2.HorizontalScrollbarSize = 10;
            this.metroPanel2.Location = new System.Drawing.Point(3, 241);
            this.metroPanel2.Name = "metroPanel2";
            this.metroPanel2.Size = new System.Drawing.Size(248, 438);
            this.metroPanel2.TabIndex = 2;
            this.metroPanel2.VerticalScrollbarBarColor = true;
            this.metroPanel2.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel2.VerticalScrollbarSize = 10;
            // 
            // btnRunReport
            // 
            this.btnRunReport.Location = new System.Drawing.Point(135, 137);
            this.btnRunReport.Name = "btnRunReport";
            this.btnRunReport.Size = new System.Drawing.Size(113, 38);
            this.btnRunReport.TabIndex = 6;
            this.btnRunReport.Text = "&Run";
            this.btnRunReport.UseSelectable = true;
            this.btnRunReport.Click += new System.EventHandler(this.btnRunReport_Click);
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Location = new System.Drawing.Point(7, 102);
            this.dtpEndDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(241, 29);
            this.dtpEndDate.TabIndex = 5;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(7, 80);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(65, 19);
            this.metroLabel2.TabIndex = 4;
            this.metroLabel2.Text = "End Date:";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Location = new System.Drawing.Point(7, 38);
            this.dtpStartDate.MinimumSize = new System.Drawing.Size(0, 29);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(241, 29);
            this.dtpStartDate.TabIndex = 3;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(7, 16);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(71, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Start Date:";
            // 
            // metroPanel1
            // 
            this.metroPanel1.Controls.Add(this.lstBoxReportType);
            this.metroPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.metroPanel1.HorizontalScrollbarBarColor = true;
            this.metroPanel1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroPanel1.HorizontalScrollbarSize = 10;
            this.metroPanel1.Location = new System.Drawing.Point(3, 3);
            this.metroPanel1.Name = "metroPanel1";
            this.metroPanel1.Size = new System.Drawing.Size(248, 232);
            this.metroPanel1.TabIndex = 1;
            this.metroPanel1.VerticalScrollbarBarColor = true;
            this.metroPanel1.VerticalScrollbarHighlightOnWheel = false;
            this.metroPanel1.VerticalScrollbarSize = 10;
            // 
            // lstBoxReportType
            // 
            this.lstBoxReportType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstBoxReportType.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstBoxReportType.ForeColor = System.Drawing.SystemColors.ControlText;
            this.lstBoxReportType.FormattingEnabled = true;
            this.lstBoxReportType.ItemHeight = 23;
            this.lstBoxReportType.Items.AddRange(new object[] {
            "Stock Level Report",
            "Stock Transaction Report"});
            this.lstBoxReportType.Location = new System.Drawing.Point(0, 0);
            this.lstBoxReportType.Name = "lstBoxReportType";
            this.lstBoxReportType.Size = new System.Drawing.Size(248, 232);
            this.lstBoxReportType.TabIndex = 2;
            // 
            // vReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1280, 768);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "vReports";
            this.Text = "Report Viewer";
            this.Load += new System.EventHandler(this.vReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.prcStockTransactionDetailReportGetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportStDetailDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prc_StockCountReportGetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportStCountDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.prcStockCountReportGetBindingSource)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.metroPanel3.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.metroPanel2.ResumeLayout(false);
            this.metroPanel2.PerformLayout();
            this.metroPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource prcStockTransactionDetailReportGetBindingSource;
        private ReportStDetailDataSet reportStDetailDataSet;
        private ReportStDetailDataSetTableAdapters.prc_StockTransactionDetailReportGetTableAdapter prc_StockTransactionDetailReportGetTableAdapter;
        private reportStCountDataSet reportStCountDataSet;
        private System.Windows.Forms.BindingSource prcStockCountReportGetBindingSource;
        private reportStCountDataSetTableAdapters.prc_StockCountReportGetTableAdapter prc_StockCountReportGetTableAdapter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private MetroFramework.Controls.MetroPanel metroPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private MetroFramework.Controls.MetroPanel metroPanel2;
        private MetroFramework.Controls.MetroPanel metroPanel1;
        private MetroFramework.Controls.MetroDateTime dtpEndDate;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroDateTime dtpStartDate;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.ListBox lstBoxReportType;
        private MetroFramework.Controls.MetroButton btnRunReport;
        private Microsoft.Reporting.WinForms.ReportViewer rptStockCount;
        private Microsoft.Reporting.WinForms.ReportViewer rptStockTransactionDetail;
        private System.Windows.Forms.BindingSource prc_StockCountReportGetBindingSource;
    }
}