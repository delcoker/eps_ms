﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace EPSInventoryManager
{
    public partial class frmPartnerDetails : MetroForm
    {
        public frmPartnerDetails()
        {
            InitializeComponent();
            txtPartnerName.Focus();

            loadComboBoxes();
        }


        SqlCo sqls;

        public string Country_id { get; set; }
        //public string Country_name { get; set; }

        private void frmPartnerDetails_Load(object sender, EventArgs e)
        {


        }
        //load data into status and partner type controls
        public void loadComboBoxes()
        {
            SqlCo.LoadPartnerType(cbxPartnerType);
            SqlCo.loadStatus(cbxPartnerStatus);
            CountriesList();
        }

        //method to get countries list
        //load countries list into country combobox
        public DataSet CountriesList()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_CountriesGet";
            sqls.Cmd.CommandType = CommandType.StoredProcedure;
            sqls.query("@country_id", Country_id);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);
                da.Fill(ds, "countries_list");
                DataTable dt = new DataTable();
                //da.Fill(ds, "countries_list");
                cbxPartnerCountry.DataSource = ds.Tables[0].DefaultView;
                cbxPartnerCountry.ValueMember = "country_name";
                cbxPartnerCountry.DisplayMember = "country_name";


                return ds;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }

        }



        //Get country list from using the CultureInfo class

        //Get country list into country drop down list
        //public List<string> CountryNameList()
        //    {

        //    List<string> cultureList = new List<string>();

        //    //Array to hold list of cultures(countries) from the CultureInfo class
        //    CultureInfo[] cultures = CultureInfo.GetCultures(CultureTypes.SpecificCultures);

        //    foreach (CultureInfo getculture in cultures)
        //        {

        //        RegionInfo getRegionInfo;
        //        getRegionInfo = new RegionInfo(getculture.LCID);
        //        if (!(cultureList.Contains(getRegionInfo.EnglishName)))
        //            {
        //            cultureList.Add(getRegionInfo.EnglishName);
        //            cbxPartnerCountry.Items.Add(getRegionInfo.EnglishName);
        //            }
        //        }
        //    cultureList.Sort();
        //    return cultureList;
        //    }


        private void btnAddPartner_Click(object sender, EventArgs e)
        {
            if (txtPartnerName.Text.Equals(string.Empty))
            {
                MessageBox.Show("Please enter a Partner name.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPartnerName.Focus();
                return;
            }
            //cBusinessPartner bPartner = new cBusinessPartner();



            cBusinessPartner bPartnerAdd = new cBusinessPartner("0", txtPartnerName.Text, cbxPartnerType.SelectedValue.ToString(),
                txtPartnerEmail.Text, cbxPartnerCountry.SelectedValue.ToString(), txtPartnerCity.Text, txtPartnerPhone.Text,
                txtPartnerAddress.Text, txtPartnerRemarks.Text, cbxPartnerStatus.SelectedValue.ToString(), true);

            if (bPartnerAdd.PartnerCheck())
            {
                MessageBox.Show("Partner already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!bPartnerAdd.BusinessPartnerSave())
            {

                MessageBox.Show("Could not save.");
                return;
            }

            else
            {
                MessageBox.Show("Saved successfully.");
            }
            txtPartnerName.Focus();
            txtPartnerName.Clear();
            txtPartnerAddress.Clear();
            txtPartnerCity.Clear();
            txtPartnerEmail.Clear();
            txtPartnerPhone.Clear();
            txtPartnerRemarks.Clear();
            cbxPartnerCountry.SelectedIndex = 0;
            cbxPartnerStatus.SelectedIndex = 0;
            if (cbxPartnerType.SelectedValue.ToString() == "Customer")
            {
                cbxPartnerType.SelectedIndex = 1;
            }


        }

        private void btnCustCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }


}

