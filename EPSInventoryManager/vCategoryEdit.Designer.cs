﻿namespace EPSInventoryManager
{
    partial class vCategoryEdit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.categorytableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCatRemarks = new MetroFramework.Controls.MetroTextBox();
            this.cbxCatStatus = new MetroFramework.Controls.MetroComboBox();
            this.txtCategoryName = new MetroFramework.Controls.MetroTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnAddCategory = new MetroFramework.Controls.MetroButton();
            this.dgvCategoryView = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.categorytableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoryView)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // categorytableLayoutPanel1
            // 
            this.categorytableLayoutPanel1.ColumnCount = 1;
            this.categorytableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 76.35904F));
            this.categorytableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.categorytableLayoutPanel1.Controls.Add(this.dgvCategoryView, 0, 1);
            this.categorytableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.categorytableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.categorytableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.categorytableLayoutPanel1.Name = "categorytableLayoutPanel1";
            this.categorytableLayoutPanel1.RowCount = 2;
            this.categorytableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 38.98477F));
            this.categorytableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 61.01523F));
            this.categorytableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.categorytableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.categorytableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.categorytableLayoutPanel1.Size = new System.Drawing.Size(641, 423);
            this.categorytableLayoutPanel1.TabIndex = 0;
            this.categorytableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.categorytableLayoutPanel1_Paint);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.22107F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.77893F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(637, 160);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.93814F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.06186F));
            this.tableLayoutPanel1.Controls.Add(this.txtCatRemarks, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbxCatStatus, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCategoryName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(2, 2);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(430, 156);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // txtCatRemarks
            // 
            // 
            // 
            // 
            this.txtCatRemarks.CustomButton.Image = null;
            this.txtCatRemarks.CustomButton.Location = new System.Drawing.Point(240, 2);
            this.txtCatRemarks.CustomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCatRemarks.CustomButton.Name = "";
            this.txtCatRemarks.CustomButton.Size = new System.Drawing.Size(53, 53);
            this.txtCatRemarks.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCatRemarks.CustomButton.TabIndex = 1;
            this.txtCatRemarks.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCatRemarks.CustomButton.UseSelectable = true;
            this.txtCatRemarks.CustomButton.Visible = false;
            this.txtCatRemarks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCatRemarks.Lines = new string[0];
            this.txtCatRemarks.Location = new System.Drawing.Point(100, 81);
            this.txtCatRemarks.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCatRemarks.MaxLength = 32767;
            this.txtCatRemarks.Multiline = true;
            this.txtCatRemarks.Name = "txtCatRemarks";
            this.txtCatRemarks.PasswordChar = '\0';
            this.txtCatRemarks.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCatRemarks.SelectedText = "";
            this.txtCatRemarks.SelectionLength = 0;
            this.txtCatRemarks.SelectionStart = 0;
            this.txtCatRemarks.ShortcutsEnabled = true;
            this.txtCatRemarks.Size = new System.Drawing.Size(328, 72);
            this.txtCatRemarks.TabIndex = 2;
            this.txtCatRemarks.UseSelectable = true;
            this.txtCatRemarks.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCatRemarks.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // cbxCatStatus
            // 
            this.cbxCatStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbxCatStatus.FormattingEnabled = true;
            this.cbxCatStatus.ItemHeight = 23;
            this.cbxCatStatus.Location = new System.Drawing.Point(100, 42);
            this.cbxCatStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cbxCatStatus.Name = "cbxCatStatus";
            this.cbxCatStatus.Size = new System.Drawing.Size(328, 29);
            this.cbxCatStatus.TabIndex = 1;
            this.cbxCatStatus.UseSelectable = true;
            // 
            // txtCategoryName
            // 
            // 
            // 
            // 
            this.txtCategoryName.CustomButton.Image = null;
            this.txtCategoryName.CustomButton.Location = new System.Drawing.Point(272, 2);
            this.txtCategoryName.CustomButton.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCategoryName.CustomButton.Name = "";
            this.txtCategoryName.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.txtCategoryName.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCategoryName.CustomButton.TabIndex = 1;
            this.txtCategoryName.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCategoryName.CustomButton.UseSelectable = true;
            this.txtCategoryName.CustomButton.Visible = false;
            this.txtCategoryName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCategoryName.Lines = new string[0];
            this.txtCategoryName.Location = new System.Drawing.Point(100, 3);
            this.txtCategoryName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCategoryName.MaxLength = 32767;
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.PasswordChar = '\0';
            this.txtCategoryName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCategoryName.SelectedText = "";
            this.txtCategoryName.SelectionLength = 0;
            this.txtCategoryName.SelectionStart = 0;
            this.txtCategoryName.ShortcutsEnabled = true;
            this.txtCategoryName.Size = new System.Drawing.Size(328, 33);
            this.txtCategoryName.TabIndex = 0;
            this.txtCategoryName.UseSelectable = true;
            this.txtCategoryName.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCategoryName.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 39);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Status:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(2, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(83, 13);
            this.label8.TabIndex = 55;
            this.label8.Text = "Category Name:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 78);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 13);
            this.label7.TabIndex = 58;
            this.label7.Text = "Description:";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.btnAddCategory, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(436, 2);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(199, 156);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // btnAddCategory
            // 
            this.btnAddCategory.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnAddCategory.Location = new System.Drawing.Point(2, 81);
            this.btnAddCategory.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAddCategory.Name = "btnAddCategory";
            this.btnAddCategory.Size = new System.Drawing.Size(195, 72);
            this.btnAddCategory.TabIndex = 0;
            this.btnAddCategory.Text = "&Save";
            this.btnAddCategory.UseSelectable = true;
            this.btnAddCategory.Click += new System.EventHandler(this.btnAddCategory_Click);
            // 
            // dgvCategoryView
            // 
            this.dgvCategoryView.AllowUserToAddRows = false;
            this.dgvCategoryView.AllowUserToDeleteRows = false;
            this.dgvCategoryView.AllowUserToOrderColumns = true;
            this.dgvCategoryView.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.dgvCategoryView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategoryView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvCategoryView.Location = new System.Drawing.Point(2, 166);
            this.dgvCategoryView.Margin = new System.Windows.Forms.Padding(2);
            this.dgvCategoryView.Name = "dgvCategoryView";
            this.dgvCategoryView.ReadOnly = true;
            this.dgvCategoryView.RowTemplate.Height = 33;
            this.dgvCategoryView.Size = new System.Drawing.Size(637, 255);
            this.dgvCategoryView.TabIndex = 500;
            this.dgvCategoryView.TabStop = false;
            this.dgvCategoryView.Click += new System.EventHandler(this.dgvCategoryView_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.categorytableLayoutPanel1);
            this.panel1.Location = new System.Drawing.Point(12, 69);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(641, 423);
            this.panel1.TabIndex = 1;
            // 
            // vCategoryEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 504);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "vCategoryEdit";
            this.Padding = new System.Windows.Forms.Padding(10, 60, 10, 10);
            this.Text = "Category Edit";
            this.Load += new System.EventHandler(this.vCategoryEdit_Load);
            this.categorytableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategoryView)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel categorytableLayoutPanel1;
        private System.Windows.Forms.DataGridView dgvCategoryView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label8;
        private MetroFramework.Controls.MetroTextBox txtCategoryName;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroComboBox cbxCatStatus;
        private MetroFramework.Controls.MetroTextBox txtCatRemarks;
        private System.Windows.Forms.Label label7;
        private MetroFramework.Controls.MetroButton btnAddCategory;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel1;
    }
}