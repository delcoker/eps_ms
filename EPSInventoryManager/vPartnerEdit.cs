﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace EPSInventoryManager
{
    public partial class vPartnerEdit : MetroForm
    {
        SqlCo sqls;

        public string Country_id { get; private set; }

        public vPartnerEdit()
        {
            InitializeComponent();
            SqlCo.loadStatus(cbxPartnerStatus);
            SqlCo.LoadPartnerType(cbxPartnerType);


        }

        //method to get countries list

        //load countries list into country combobox
        public DataSet CountriesList()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_CountriesGet";
            sqls.Cmd.CommandType = CommandType.StoredProcedure;
            sqls.query("@country_id", Country_id);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);
                da.Fill(ds, "countries_list");
                DataTable dt = new DataTable();
                da.Fill(ds, "countries_list");
                cbxPartnerCountry.DataSource = ds.Tables[0].DefaultView;
                cbxPartnerCountry.ValueMember = "country_name";
                //cbxPartnerCountry.DisplayMember = "Country";
                return ds;

            }

            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }

        }




        private void btnCustCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void vPartnerEdit_Load(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            cBusinessPartner cbp = new cBusinessPartner();
            this.CountriesList();
            try
            {
                cbp.Business_partnerId = "0";

                ds = cbp.BusinessPartnerGet();

                dgvEditPartner.DataSource = ds.Tables[0];
                //Set Column headers for data from dataset
                dgvEditPartner.Columns["business_partnerId"].HeaderText = "Partner Id";
                dgvEditPartner.Columns["business_partnerId"].Visible = false;
                dgvEditPartner.Columns["business_partnerName"].HeaderText = "Partner Name";
                dgvEditPartner.Columns["partner_type"].HeaderText = "Partner Type";
                dgvEditPartner.Columns["address_"].HeaderText = "Address";
                dgvEditPartner.Columns["email"].HeaderText = "Email";
                dgvEditPartner.Columns["phone_number"].HeaderText = "Phone Number";
                dgvEditPartner.Columns["country"].HeaderText = "Country";
                dgvEditPartner.Columns["city"].HeaderText = "City";
                dgvEditPartner.Columns["remarks"].HeaderText = "Remarks";
                dgvEditPartner.Columns["status_"].HeaderText = "Status";
                dgvEditPartner.Columns["created_at"].HeaderText = "Created at";
                dgvEditPartner.Columns["updated_at"].HeaderText = "Updated at";



                dgvEditPartner.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
                dgvEditPartner.AllowUserToResizeColumns = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                ds.Dispose();
            }
            //pEdt.Enabled = false;
        }

        private void btnSaveEditPartner_Click(object sender, EventArgs e)
        {
            cBusinessPartner partnerEdit = new cBusinessPartner();
            try
            {
                partnerEdit.Business_partnerId = dgvEditPartner["business_partnerId", dgvEditPartner.CurrentCell.RowIndex].Value.ToString();
            }
            catch (Exception)
            {
                //MessageBox.Show(ex.Message);

                MessageBox.Show("Please check your entry", "Saving Partner", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (txtPartnerName.Text == string.Empty)
            {
                MessageBox.Show("Please select a record to edit.", "Message.", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                return;

            }
            partnerEdit.Business_partnerName = txtPartnerName.Text;
            partnerEdit.Description = txtPartnerRemarks.Text;
            partnerEdit.City = txtPartnerCity.Text;
            partnerEdit.Email = txtPartnerEmail.Text;
            partnerEdit.PhoneNumber = txtPartnerPhone.Text;
            partnerEdit.Address = txtPartnerAddress.Text;
            partnerEdit.Business_partnerType = cbxPartnerType.SelectedValue.ToString();
            partnerEdit.status_ = cbxPartnerStatus.SelectedValue.ToString();
            partnerEdit.Country_name = cbxPartnerCountry.SelectedValue.ToString();

            if (!partnerEdit.BusinessPartnerSave())
            {
                MessageBox.Show("Could not save!", "Saving Partner", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else
            {
                MessageBox.Show("Saved successfully.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                vPartnerEdit_Load(sender, e);
                txtPartnerName.Clear();
                txtPartnerName.Focus();
            }

            //dgvItemEdit.Refreshe();

            vPartnerEdit_Load(sender, e);

        }


        private void dgvEditPartner_Click(object sender, EventArgs e)
        {
            try
            {
                txtPartnerAddress.Text = (dgvEditPartner["address_", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            }
            catch (Exception ex)
            {

                MessageBox.Show("You have no items to view\n" + ex.Message);
            }

            txtPartnerPhone.Text = (dgvEditPartner["phone_number", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            cbxPartnerType.SelectedValue = (dgvEditPartner["partner_type", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            cbxPartnerStatus.SelectedValue = (dgvEditPartner["status_", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            txtPartnerRemarks.Text = (dgvEditPartner["remarks", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            cbxPartnerCountry.SelectedValue = (dgvEditPartner["country", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            txtPartnerName.Text = (dgvEditPartner["business_partnerName", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            txtPartnerCity.Text = (dgvEditPartner["city", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());
            txtPartnerEmail.Text = (dgvEditPartner["email", dgvEditPartner.CurrentCell.RowIndex].Value.ToString());

            //vPartnerEdit_Load(sender, e);
        }

    }
}
