using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace EPSInventoryManager                          // DAVE change this namespace to your project name space
{
    public class cItem
    {

        //declare member variables
        private string mItemId;
        private int mQuantity;
        private string mItemName;
        //private string mItemPicUrl;
        private string mCategoryId;
        private int mMinimum_stock_level;
        private string mStatus;
        private string mDescription_;
        private string mGeneratedId;
        private decimal mPrice;
        private string mUnits;




        private SqlCo sqls;

        public cItem() { }

        public cItem(string ItemId, string ItemName, decimal Price, string Units, string Category_id, int MinStock, string status, string Description)
        {
            mItemId = ItemId;
            mItemName = ItemName;
            mPrice = Price;
            mUnits = Units;
            mCategoryId = Category_id;
            mMinimum_stock_level = MinStock;
            mStatus = status;
            mDescription_ = Description;
        }

        public cItem(string ItemId, string ItemName, decimal Price, string Units, string Category_id, int MinStock, string status,
            string Description, bool GeneratedId)
        {
            mItemId = ItemId;
            mItemName = ItemName;
            mPrice = Price;
            mUnits = Units;
            mCategoryId = Category_id;
            mMinimum_stock_level = MinStock;
            mStatus = status;
            mDescription_ = Description;
            if (GeneratedId)
            {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
                mGeneratedId = epoch + SqlCo.RandomString(3);
            }
        }

        public cItem(string ItemId, int Quantity, bool GeneratedId)
        {
            mItemId = ItemId;
            mQuantity = Quantity;
            if (GeneratedId)
            {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
                mGeneratedId = epoch + SqlCo.RandomString(3);
            }
        }

        public bool ItemCheck()
        {
            // Del's class to quickly connect
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemCheck";

            sqls.query("@item_name", ItemName);

            SqlDataReader dr = sqls.Cmd.ExecuteReader();
            try
            {
                while (dr.Read())
                {
                    if (dr.GetSqlValue(0).ToString() == "1")
                        return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
            return false;
        }

        public DataSet ItemsGet()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemsGet";

            sqls.query("@item_id", ItemId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "item");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public DataSet ItemQuantityGet()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemQuantityGet";

            sqls.query("@item_id", ItemId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "Item");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public DataSet ItemGetActive()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemGetActive";

            sqls.query("@item_id", ItemId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "Item");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public DataSet prc_ItemGetActiveWithCategoryID()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemGetActiveWithCategoryID";

            sqls.query("@item_id", ItemId);
            sqls.query("@item_category_id", CategoryId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "Item");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public DataSet ItemGetActiveWithStock()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemGetActiveWithStock";

            sqls.query("@item_id", ItemId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "Item");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }






        public DataSet ItemGet()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemsGet";

            sqls.query("@item_id", ItemId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "Item");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public bool ItemSave()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemSave";

            sqls.query("@item_id", ItemId);
            sqls.query("@item_name", ItemName);
            sqls.query("@category_id", CategoryId);
            sqls.query("@price", Price);
            sqls.query("@units", Units);
            sqls.query("@minimum_stock_level", MinimumStockLevel);
            sqls.query("@description", Description);
            sqls.query("@status_", Status_);
            sqls.query("@generated_item_id", GeneratedId);


            try
            {
                sqls.Cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public bool ItemQuantitySave()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_ItemQuantitySave";

            sqls.query("@item_id", ItemId);
            sqls.query("@quantity", Quantity);
            //sqls.query("@generated_item_id", GeneratedId);


            try
            {
                sqls.Cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }



        public DataSet TotalItemDebitsAndCredits()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_StockDebitsFromTransaction";

            sqls.query("@item_id", ItemId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "StockPurchase");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        //Accessor & Mutator methods for item class
        public string ItemId
        {
            get { return mItemId; }
            set { mItemId = value; }
        }


        public string GeneratedId
        {
            get { return mGeneratedId; }
            set { mGeneratedId = value; }
        }

        public string ItemName
        {
            get { return mItemName; }
            set { mItemName = value; }
        }

        //public decimal ItemPrice
        //{
        //    get { return mItemPrice; }
        //    set { mItemPrice = value; }
        //}

        public int MinimumStockLevel
        {
            get { return mMinimum_stock_level; }
            set { mMinimum_stock_level = value; }
        }
        public string Description
        {
            get { return mDescription_; }
            set { mDescription_ = value; }
        }
        public string Status_
        {
            get { return mStatus; }
            set { mStatus = value; }
        }

        public string CategoryId
        {
            get { return mCategoryId; }
            set { mCategoryId = value; }
        }

        public decimal Price
        {
            get { return mPrice; }
            set { mPrice = value; }
        }

        //public decimal Price { get => mPrice; set => mPrice = value; }
        public string Units { get { return mUnits; } set { mUnits = value; } }
        public int Quantity { get { return mQuantity; } set { mQuantity = value; } }
        } 
}

