
If Exists(Select * from sysobjects where name='prc_CategoryGet' and xtype='P')
   Drop Procedure prc_ItemCategoryGet

GO

Create Procedure prc_CategoryGet  -- 0
(
    @category_id	varchar(100),
    @category_name	varchar(100),
    @remarks		text
    ,@status_		varchar(10)
    ,@generated_item_id varchar(100)	= '0'
)
As

    If (@category_id = '0')
    
        Select 			
            category_id
            ,category_name
            ,status_ 
            ,remarks
            ,created_at		
            ,updated_at		

        From category
        where category_id = @category_id
        Order by category_name Asc

    /*Else
        Select 
            category_id
            ,category_name
            ,status_ 
            ,remarks
            ,created_at		
            ,updated_at	
        From category
        where vendor_id = @vendor_id
        Order by category_name Asc*/
