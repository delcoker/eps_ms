﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPSInventoryManager
{
    public class cUser
    {
        private string mUserId;
        private string mUserName;
        private string mPassword;
        private string mGeneratedUserId;
        private string mEmail;
        private string mPhoneNumber;
        private string mRemarks;
        private string mStatus;
        private SqlCo sqls;

        public cUser()
        { }

        //This is for Login
        public cUser(string UserId, string Username, string Password, bool GeneratedUserId)
        {
            mUserName = Username;
            mPassword = Password;
            mUserId = UserId;

            if (GeneratedUserId)
            {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
                mGeneratedUserId = epoch + SqlCo.RandomString(3);
            }
        }


        public cUser(string Username, string Password, bool GeneratedUserId, string Email, string PhoneNumber, string Remarks, string Status)
        {
            mUserName = Username;
            mPassword = Password;
            mEmail = Email;
            mPhoneNumber = PhoneNumber;
            mRemarks = Remarks;

            if (GeneratedUserId)
            {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
                mGeneratedUserId = epoch + SqlCo.RandomString(3);
            }
        }

        //Validate Users
        public bool UserCheck()
        {
            // connect to db
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_UserCheck";

            sqls.query("@user_name", UserName);
            sqls.query("@password", Password);


            try
            {
                SqlDataReader dr = sqls.Cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr.GetSqlValue(0).ToString() == "1")
                        return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
            return false;
        }



        public string UserName
        {
            get
            {
                return mUserName;
            }

            set
            {
                mUserName = value;
            }
        }

        public string Password
        {
            get
            {
                return mPassword;
            }

            set
            {
                mPassword = value;
            }
        }

        public string GeneratedUserId
        {
            get
            {
                return mGeneratedUserId;
            }

            set
            {
                mGeneratedUserId = value;
            }
        }

        public string Email
        {
            get
            {
                return mEmail;
            }

            set
            {
                mEmail = value;
            }
        }

        public string Remarks
        {
            get
            {
                return mRemarks;
            }

            set
            {
                mRemarks = value;
            }
        }

        public string Status
        {
            get
            {
                return mStatus;
            }

            set
            {
                mStatus = value;
            }
        }

        public string UserId
        {
            get
            {
                return mUserId;
            }

            set
            {
                mUserId = value;
            }
        }
    }
}
