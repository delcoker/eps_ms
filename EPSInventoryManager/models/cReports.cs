﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EPSInventoryManager
{
    public class cReports
    {
        private SqlCo sqls;

        private string mStockTopUPID;
        private string mUserID;    // basically person you're getting info about
        private string mPaymentType;
        private string mPaymentMethod;
        private string mTransactionPointId;
        private string mStartDate;
        private string mEndDate;
        private string mUserName;
        private string mReportOnCashier;
        private string mVendorName;
        private string mVendorID;
        private string mTransactionPointName;
        private string mEveryItem;

        public cReports()
        {
            mStockTopUPID = "";
            mPaymentType = "";
            mPaymentMethod = "";
            mTransactionPointId = "";
            mTransactionPointName = "";
        }

        public string StartDate
        {
            get { return mStartDate; }
            set { mStartDate = value; }
        }
        public string VendorID
        {
            get { return mVendorID; }
            set { mVendorID = value; }
        }

        public string StockTopUPID
        {
            get { return mStockTopUPID; }
            set { mStockTopUPID = value; }
        }

        public string TransactionPointName
        {
            get { return mTransactionPointName; }
            set { mTransactionPointName = value; }
        }

        public string ReportOnCashier
        {
            get { return mReportOnCashier; }
            set { mReportOnCashier = value; }
        }

        public string EndDate
        {
            get { return mEndDate; }
            set { mEndDate = value; }
        }

        public string Username
        {
            get { return mUserName; }
            set { mUserName = value; }
        }

        public string UserID
        {
            get { return mUserID; }
            set { mUserID = value; }
        }

        public string VendorName
        {
            get { return mVendorName; }
            set { mVendorName = value; }
        }

        public DataSet StockTopUpReportGet()
        {
            SqlCo sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_StockTopUpReportGet";

            sqls.query("@UserID", UserID);
            sqls.query("@VendorID", VendorID);
            sqls.query("@PaymentMethod", PaymentMethod);
            sqls.query("@EveryItem", EveryItem);
            sqls.query("@StartDate", StartDate);
            sqls.query("@EndDate", EndDate);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);
                sqls.Con.Close();

                da.Fill(ds, "StockTopUpReport");
                return ds;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }


        public DataSet StockTransactionDetailReportGet()
        {
            SqlCo sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_StockTransactionDetailReportGet";

            sqls.query("@UserID", UserID);
            sqls.query("@StartDate", StartDate);
            sqls.query("@EndDate", EndDate);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);
                sqls.Con.Close();

                da.Fill(ds, "ReportTrans");
                return ds;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public DataSet SaleReportGet()
        {
            SqlCo sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_SaleReportGet";

            sqls.query("@UserID", UserID);
            sqls.query("@PaymentType", PaymentType);
            sqls.query("@PaymentMethod", PaymentMethod);
            sqls.query("@TransactionPointID", TransactionPointID);
            sqls.query("@StartDate", StartDate);
            sqls.query("@EndDate", EndDate);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);
                sqls.Con.Close();

                da.Fill(ds, "Report");
                return ds;
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        public string PaymentType
        {
            get { return mPaymentType; }
            set { mPaymentType = value; }
        }

        public string PaymentMethod
        {
            get { return mPaymentMethod; }
            set { mPaymentMethod = value; }
        }

        public string TransactionPointID
        {
            get { return mTransactionPointId; }
            set { mTransactionPointId = value; }
        }

        //public sos_pos.SqlCo Sqls
        //{
        //    get { return sqls; }
        //    set { sqls = value; }
        //}

        public string EveryItem
        {
            get { return mEveryItem; }
            set { mEveryItem = value; }
        }
    }
}
