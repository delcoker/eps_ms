readme


1)	start with the "create table category.sql"

2)	then try to save an item with the stored procedure prc_CategorySave.sql  (it should work as is)

3)	add cSqlCo.cs (change the namespace for this and all .cs files)

4)	add cItemCategory.cs (look at this file and make sure the instance variables match what the fields in the database and should start with a small m)

	-	make sure the these instance variables are private

	-	e.g.  	private string mItemCategoryId;
		        private string mItemCategoryName;
		        private string mVendorId;
		        private string mInstitutionId;
		        private string mStatus;
		        private string mDateCreated;
		        private string mGeneratedId;

	- 	make sure the accessor methods are public

		e.g.	public string ItemCategoryId
		        {
		            get { return mItemCategoryId; }
		            set { mItemCategoryId = value; }
		        }

		        public string ItemCategoryName
		        {
		            get { return mItemCategoryName; }
		            set { mItemCategoryName = value; }
		        }

5)	- make sure the queries that link to the stored procedures have the same parameter name
		
		e.g. 	public bool ItemCategorySave()
		       	- 	@item_category_name -> this should match what is in the prc_CategorySave.sql paraments as @category_name

		       	so check the other paremeter



6)	now link the add button to this to save procedure and make it save into the db