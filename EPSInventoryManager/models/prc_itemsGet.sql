
If Exists(Select * from sysobjects where name='prc_ItemsGet' and xtype='P')
   Drop Procedure prc_ItemsGet

GO

Create Procedure prc_ItemsGet  -- 0
(
   @item_id varchar(100) = '0' --initialise the default value for the id field 
)
As

    If (@item_id = '0')		--search based on condition 
    
		Select  item_id,
				item_name,
				category_id,
				minimum_stock_level,
				description_,
				status_,
				created_at,
				updated_at

		From item
		Order by item_name Asc

	Else
		Select 
				item_id,
				item_name,
				category_id,
				minimum_stock_level,
				description_,
				status_,
				created_at,
				updated_at

		From item
		where item_id = @item_id
		Order by item_name Asc


	--	,created_at = Convert(varchar(12), created_at,106)