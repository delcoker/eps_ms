using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace EPSInventoryManager                          // DAVE change this namespace to your project name space
{
    public class cInventoryTransactionDetail
    {

        //declare member variables
        private string mIventoryDetailId;
        private string mInventoryID;
        private string mItemID;
        private int mQuantity;
        private decimal mPrice;
        private string mDateOfManufacture;
        private string mDateOfExpiry;
        private string mPartNumber;
        //private string mBusinessPartnerID;
        private string mLocation;
        private string mRemarks;
        private string mGeneratedId;



        private SqlCo sqls;

        public cInventoryTransactionDetail() { }

        public cInventoryTransactionDetail(string InventoryDetailId,  string InventoryID, string ItemID, int Quantity, decimal Price, string DateOfManu, string DateofExp, string PartNumber,
            /*string BusinessPartnerId,*/ string Location, string Remarks)
        {
            mIventoryDetailId = InventoryDetailId;
            mItemID = ItemID;
            mInventoryID = InventoryID;
            mQuantity = Quantity;
            mPrice = Price;
            mDateOfManufacture = DateOfManufacture;
            mDateOfExpiry = DateOfManufacture;
            mPartNumber = PartNumber;
            //mBusinessPartnerID = BusinessPartnerID;
            mLocation = Location;
            mRemarks = Remarks;
        }

        public cInventoryTransactionDetail(string InventoryDetailId, string InventoryID, string ItemID, int Quantity, decimal Price, string DateOfManufacture, string DateOfExpiry, 
            string PartNumber, /*string BusinessPartnerID,*/ string Location, string Remarks, bool GeneratedId)
        {
            mIventoryDetailId = InventoryDetailId;
            mItemID = ItemID;
            mInventoryID = InventoryID;
            mQuantity = Quantity;
            mPrice = Price;
            mDateOfManufacture = DateOfManufacture;
            mDateOfExpiry = DateOfManufacture;
            mPartNumber = PartNumber;
            //mBusinessPartnerID = BusinessPartnerID;
            mLocation = Location;
            mRemarks = Remarks;
            
            if (GeneratedId)
            {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
                mGeneratedId = epoch + SqlCo.RandomString(3);
            }
        }

        //public bool ItemCheck()
        //{
        //    // Del's class to quickly connect
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemCheck";

        //    sqls.query("@item_name", ItemName);

        //    SqlDataReader dr = sqls.Cmd.ExecuteReader();
        //    try
        //    {
        //        while (dr.Read())
        //        {
        //            if (dr.GetSqlValue(0).ToString() == "1")
        //                return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //    return false;
        //}

        //public DataSet ItemQuantityGet()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemQuantityGet";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        //public DataSet ItemGetActive()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemGetActive";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        //public DataSet prc_ItemGetActiveWithCategoryID()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemGetActiveWithCategoryID";

        //    sqls.query("@item_id", ItemId);
        //    sqls.query("@item_category_id", CategoryId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        //public DataSet ItemGetActiveWithStock()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemGetActiveWithStock";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}






        //public DataSet ItemGet()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemGet";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}


        public bool InventoryTransactionDetailSave()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_InventoryDetailSave";

            sqls.query("@inventory_detail_id", IventoryDetailId);
            sqls.query("@item_id", ItemID);
            sqls.query("@inventory_id", InventoryID);
            sqls.query("@quantity", Quantity);
            sqls.query("@price", Price);
            sqls.query("@d_of_manufac", DateOfManufacture);
            sqls.query("@exp_date", DateOfExpiry);
            sqls.query("@part_number", PartNumber);
            //sqls.query("@business_partner_id", BusinessPartnerID);
            sqls.query("@location", Location);
            sqls.query("@remarks", Remarks);
            sqls.query("@generated_item_id", GeneratedId);


            try
            {
                sqls.Cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        //public bool ItemQuantitySave()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemQuantitySave";

        //    sqls.query("@item_id", ItemId);


        //    try
        //    {
        //        sqls.Cmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        //public DataSet TotalItemDebitsAndCredits()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_StockDebitsFromTransaction";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "StockPurchase");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}


        public string GeneratedId
        {
            get { return mGeneratedId; }
            set { mGeneratedId = value; }
        }


        public string IventoryDetailId { get { return mIventoryDetailId; } set { mIventoryDetailId = value; } }
        public string InventoryID { get { return mInventoryID; } set { mInventoryID = value; } }
        public int Quantity { get { return mQuantity; } set { mQuantity = value; } }
        public decimal Price { get { return mPrice; } set { mPrice = value; } }
        public string DateOfManufacture { get { return mDateOfManufacture; } set { mDateOfManufacture = value; } }
        public string DateOfExpiry { get { return mDateOfExpiry; } set { mDateOfExpiry = value; } }
        public string PartNumber { get { return mPartNumber; } set { mPartNumber = value; } }
        public string Location { get { return mLocation; } set { mLocation = value; } }
        public string Remarks { get { return mRemarks; } set { mRemarks = value; } }
        public string ItemID { get { return mItemID; } set { mItemID = value; } }
    }
}

