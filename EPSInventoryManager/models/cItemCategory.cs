using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace EPSInventoryManager              // DAVE change this namespace to your project name space
{
    public class cItemCategory          
    {
        // these are you instance variables => these are actually the properties
        // to differentiate them from other variables i make them start with a small m
        // the typically represent the columns in your tables 
        // for each of them you have to create a pulbic accessor method
        private string mCategoryId;
        private string mCategoryName;
        private string mRemarks;
        private string mStatus;
        private string mDateCreated;
        private string mGeneratedId;

        private SqlCo sqls;


        // so now you create some constructors, 2 is normally enough

            // this is the default constructor -- ok got it
        public cItemCategory(/*parameters go here, right? - yes but not for the default constructor. -- got it, it can be empty*/ )
            {  /*instructions go here, right?*/}  

        // we will not mainly be using this one
        public cItemCategory(string ItemCategoryID, string CategoryName, string Remarks, string Status)
        {
            mCategoryId = ItemCategoryID;
            mCategoryName = CategoryName;
            mRemarks = Remarks;
            mStatus = Status;
        }

        // we will mainly be using this one 
        public cItemCategory(string ItemCategoryID, string CategoryName, string Remarks, string Status, bool GenerateID)
        {
            mCategoryId = ItemCategoryID;
            mCategoryName = CategoryName;
            mRemarks = Remarks;
            mStatus = Status;
            if (GenerateID)// this should always be in all the other classes
            {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
                mGeneratedId = epoch + SqlCo.RandomString(3);
            }
        }

        // these are the methods that actually connect to the db through the stored procedure --
        public DataSet CategoryGetActive()
        {
            sqls = new SqlCo();
            sqls.openConnection();

            
            sqls.Cmd.CommandText = "prc_CategoryGetActive";

            sqls.query("@category_id", CategoryId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "Category");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        // get all users or one particular user based on id 
        public DataSet CategoryGet()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_CategoryGet";

            sqls.query("@category_id", CategoryId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "category");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        // check if category already exists
        public bool CategoryCheck()
        {
            // Del's class to quickly connect
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_CategoryCheck";

            sqls.query("@category_name", CategoryName);

            SqlDataReader dr = sqls.Cmd.ExecuteReader();
            try
            {
                while (dr.Read())
                {
                    if (dr.GetSqlValue(0).ToString() == "1")
                        return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
            return false;
        }

        //this is method for the stored procedure we just made
        // to save a category, you need to pass 5 fields
        public bool CategorySave()
        {
            sqls = new SqlCo();     // create my special connection object
            sqls.openConnection();  //  open the connection
            sqls.Cmd.CommandText = "prc_CategorySave";  // call the stored procedure
                                                    //  and categoryId is value you are passing to the parameter
            sqls.query("@category_id", CategoryId); //  so the @category_id is the parameter that you are passing to in the stored procedure
            sqls.query("@category_name", CategoryName); 
            sqls.query("@status_", Status);             // you added an _ to the status so it has to be @status_
            sqls.query("@remarks", Remarks); //right?   yup
            sqls.query("@generated_item_id", GeneratedId);

            try
            {
                sqls.Cmd.ExecuteNonQuery();     // this will execute the stored procedure query and save the items when you pass it values
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        //These are the public accessor methods => these give you accesss to the properties -- ok
        //wait i thought these are properties? => sure you can call them properties     -- ok

        public string CategoryId
        {
            get { return mCategoryId; } 
            set { mCategoryId = value; }
        }

        public string CategoryName
        {
            get { return mCategoryName; }
            set { mCategoryName = value; }
        }
        
        // create an accessor/method for the remarks property --ok

            public string Remarks
            {
            get { return mRemarks; }
            set { mRemarks = value;}
            }
        //correct?// yes seems good --ok

        public string Status
        {
            get { return mStatus; }
            set { mStatus = value; }
        }

        public string DateCreated
        {
            get { return mDateCreated; }
            set { mDateCreated = value; }
        }

        public string GeneratedId
        {
            get { return mGeneratedId; }
            set { mGeneratedId = value; }
        }

        public SqlCo Sqls
        {
            get { return sqls; }
            set { sqls = value; }
        }
    }
}
