using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace EPSInventoryManager                          // DAVE change this namespace to your project name space
{
    public class cInventoryTransaction
    {

        //declare member variables
        private string mInventoryId;
        private string mRecieverId;
        private string mPurchaseType;
        private string mBusinessPartnerID;
        private string mTransactionType;
        private string mWayBillNumber;
        private string mDescription_;
        private string mGeneratedId;
      

        private SqlCo sqls;

        public cInventoryTransaction() { }
        
        public cInventoryTransaction(string InventoryId, string RecieverId, string PurchaseType, string BusinessPartnerID,
            string TransactionType, string WayBillNumber, string Description)
        {
            MInventoryId = InventoryId;
            mRecieverId = RecieverId;
            mPurchaseType = PurchaseType;
            mBusinessPartnerID = BusinessPartnerID;
            mTransactionType = TransactionType;
            mWayBillNumber = WayBillNumber;
            //mStatus = status;
            mDescription_ = Description;
        }

        public cInventoryTransaction(string InventoryId, string RecieverId, string PurchaseType, string BusinessPartnerID,
            string TransactionType, string WayBillNumber, string Description, /*string status, */bool GeneratedId)
        {
             mInventoryId = InventoryId;
            mRecieverId = RecieverId;
            mPurchaseType = PurchaseType;
            mBusinessPartnerID = BusinessPartnerID;
            mTransactionType = TransactionType;
            mWayBillNumber = WayBillNumber;
            //mStatus = status;
            mDescription_ = Description;
            if (GeneratedId)
            {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);
                mGeneratedId = epoch + SqlCo.RandomString(3);
            }
        }

        //public bool ItemCheck()
        //{
        //    // Del's class to quickly connect
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemCheck";

        //    sqls.query("@item_name", ItemName);

        //    SqlDataReader dr = sqls.Cmd.ExecuteReader();
        //    try
        //    {
        //        while (dr.Read())
        //        {
        //            if (dr.GetSqlValue(0).ToString() == "1")
        //                return true;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //    return false;
        //}

        //public DataSet ItemQuantityGet()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemQuantityGet";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        //public DataSet ItemGetActive()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemGetActive";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        
      



        

        
        //public DataSet ItemGet()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemGet";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "Item");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        public bool InventorySave()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_InventorySave";

            sqls.query("@inventory_id", MInventoryId);
            sqls.query("@receiver_id", RecieverId);
            sqls.query("@purchase_type", PurchaseType);
            sqls.query("@business_partner_id", BusinessPartnerID);
            sqls.query("@transaction_type", TransactionType);
            sqls.query("@waybill_no", WayBillNumber);
            sqls.query("@remarks", Description);
            //sqls.query("@status_", Status);
            sqls.query("@generated_item_id", GeneratedId);

            try
            {
                sqls.Cmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        //public bool ItemQuantitySave()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_ItemQuantitySave";

        //    sqls.query("@item_id", ItemId);


        //    try
        //    {
        //        sqls.Cmd.ExecuteNonQuery();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.Message);
        //        return false;
        //        throw ex;
        //    }
        //    finally
        //    {
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}

        //public DataSet TotalItemDebitsAndCredits()
        //{
        //    sqls = new SqlCo();
        //    sqls.openConnection();
        //    sqls.Cmd.CommandText = "prc_StockDebitsFromTransaction";

        //    sqls.query("@item_id", ItemId);

        //    DataSet ds = new DataSet();
        //    try
        //    {
        //        SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

        //        da.Fill(ds, "StockPurchase");
        //        return ds;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    finally
        //    {
        //        ds.Dispose();
        //        sqls.Con.Dispose();
        //        sqls.Con.Close();
        //    }
        //}


        public string GeneratedId
        {
            get { return mGeneratedId; }
            set { mGeneratedId = value; }
        }

        public string MInventoryId { get { return mInventoryId; } set { mInventoryId = value; } }
        public string RecieverId { get { return mRecieverId; } set { mRecieverId = value; } }
        public string PurchaseType { get { return mPurchaseType; } set { mPurchaseType = value; } }
        public string BusinessPartnerID { get { return mBusinessPartnerID; } set { mBusinessPartnerID = value; } }
        public string WayBillNumber { get { return mWayBillNumber; } set { mWayBillNumber = value; } }
        //public string Status { get => mStatus; set => mStatus = value; }
        public string Description { get { return mDescription_; } set { mDescription_ = value; } }
        public string TransactionType { get { return mTransactionType; } set { mTransactionType = value; } }
    }
}
