﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;

namespace EPSInventoryManager
    {
    public class cBusinessPartner
        {
        private string mBusiness_partnerId;
        private string mBusiness_partnerName;
        private string mBusiness_partnerType;
        private string mEmail;
        private string mPhoneNumber;
        private string mAddress; 
        private string mDescription;
        private string mCountry_name;
        private string mCity;
        private string mStatus_;
        private string mGeneratedId;
        private string mCountry_id;//for countries list

        SqlCo sqls;
        //default constructor
        public cBusinessPartner()
            {

            }

        //2nd constructor with a parameter list
        public cBusinessPartner(string Business_partnerId, string Business_partnerName, string Business_partnerType,
            string Email, string Country_name, string City, string PhoneNumber, string Address, string Description, string Status, bool GeneratedId)
            {
            mBusiness_partnerId = Business_partnerId;
            mBusiness_partnerName = Business_partnerName;
            mBusiness_partnerType = Business_partnerType;
            mEmail = Email;
            mPhoneNumber = PhoneNumber;
            mAddress = Address;
            mCity = City;
            mCountry_name = Country_name;
            mDescription = Description;
            mStatus_ = Status;
            
                if (GeneratedId)
                {
                double epoch = SqlCo.ConvertToUnixTimestamp(DateTime.Now);

                mGeneratedId = epoch + SqlCo.RandomString(3);
                }
            }


        // get all users or one particular user based on id 
        public DataSet BusinessPartnerGet()
            {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_business_PartnerGet";

            sqls.query("@business_partnerId", Business_partnerName);

            DataSet ds = new DataSet();
            try
                {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "business_partner");
                return ds;
                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
                }
            }


        public DataSet SupplierGet()
        {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_SuppliersGet";

            sqls.query("@business_partnerId", mBusiness_partnerId);

            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);

                da.Fill(ds, "supplier");
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
        }

        //method to get countries list
        //load countries list into country combobox
        public DataSet CountriesList()
            {
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_CountriesGet";
            sqls.Cmd.CommandType = CommandType.StoredProcedure;
            sqls.query("@country_id", Country_id);

            DataSet ds = new DataSet();
            try
                {
                SqlDataAdapter da = new SqlDataAdapter(sqls.Cmd);
                da.Fill(ds, "countries_list");
                DataTable dt = new DataTable();
                da.Fill(ds, "countries_list");                

                return ds;

                }
            catch (Exception ex)
                {
                throw ex;
                }
            finally
                {
                ds.Dispose();
                sqls.Con.Dispose();
                sqls.Con.Close();
                }

            }

        // Check if Business Partner already exists
        public bool PartnerCheck()
        {
            // Del's class to quickly connect
            sqls = new SqlCo();
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_PartnerCheck";

            sqls.query("@business_partnerName", Business_partnerName);
            
            try
            {
                SqlDataReader dr = sqls.Cmd.ExecuteReader();
                while (dr.Read())
                {
                    if (dr.GetSqlValue(0).ToString() == "1")
                        return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
            }
            finally
            {
                sqls.Con.Dispose();
                sqls.Con.Close();
            }
            return false;
        }



        //Method to save Partner Details into DB business_partner table
        public bool BusinessPartnerSave()
            {
            sqls = new SqlCo();     // create my special connection object
            sqls.openConnection();
            sqls.Cmd.CommandText = "prc_business_PartnerSave";  // call the stored procedure

            sqls.query("@business_partnerId", Business_partnerId);
            sqls.query("@business_partnerName", Business_partnerName);
            sqls.query("@partner_type", Business_partnerType);
            sqls.query("@email", Email);
            sqls.query("@phone_number", PhoneNumber);
            sqls.query("@address_", Address);
            sqls.query("@country", Country_name);
            sqls.query("@city", City);
            sqls.query("@remarks", Description);
            sqls.query("@status_", status_);
            sqls.query("@generated_item_id", GeneratedId);

            try
                {                
                sqls.Cmd.ExecuteNonQuery();     // this will execute the stored procedure query and save the items when you pass it values
                return true;
                }
            catch (Exception ex)
                {
                MessageBox.Show(ex.Message);
                return false;
                throw ex;
                }
            finally
                {
                sqls.Con.Dispose();
                sqls.Con.Close();
                }
            }

       
        public string Business_partnerId
            {
            get { return mBusiness_partnerId; }
            set { mBusiness_partnerId = value; }
            }

        public string Business_partnerName
            {
            get { return mBusiness_partnerName; }
            set { mBusiness_partnerName = value; }
            }

        public string Business_partnerType
            {
            get { return mBusiness_partnerType; }
            set { mBusiness_partnerType = value; }
            }

        public string Email
            {
            get { return mEmail; }
            set { mEmail = value; }
            }

        public string PhoneNumber
            {
            get { return mPhoneNumber; }
            set { mPhoneNumber = value; }
            }

        public string Address
            {
            get { return mAddress; }
            set { mAddress = value; }
            }

        public string Description
            {
            get { return mDescription; }
            set { mDescription = value; }
            }

        public string status_
            {
            get { return mStatus_; }
            set { mStatus_ = value; }
            }

        public string Country_name
            {
            get { return mCountry_name; }
            set { mCountry_name = value; }
            }

        public string City
            {
            get { return mCity; }
            set { mCity = value; }
            }
        public string GeneratedId
            {
            get { return mGeneratedId; }
            set { mGeneratedId = value; }
            }

        public string Country_id
            {
            get { return mCountry_id; }
            set { mCountry_id = value; }
            }

        }

    }
