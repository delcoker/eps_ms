﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace EPSInventoryManager
{
    public partial class vAddCategory : MetroForm
    {
        // we need the sqlco object here to create a connection
        private SqlCo sqls; // if i do it here, it makes sqls global... as much as polssible avoid global variables
        //they cause issues if you're not careful
        public vAddCategory()
        {
            // when the form is created i'll initailaize sqlco
            sqls = new SqlCo(); //you could have also done it like this => private SqlCo sqls = new SqlCo(); -- right?// depends


            InitializeComponent();
            loadData(); // after the componenets have loaded, i try to load data into the items like the combo box and datagridviews and wtv


        }

        public void loadData()
        {
            // in this case, just the combobox status... for the status too, i have 
            // functions in my sqlco file to do that for me

            //got that
            SqlCo.loadStatus(cbxStatus);
        }

        private void btnCancelCategory_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnAddCategory_Click(object sender, EventArgs e)
        {
            // now to save to the db
            // you can do some checks
            if (txtCatName.Text == string.Empty)
            {
                MessageBox.Show("Please enter a category name.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //is this ok? sure
                txtCatName.Focus();
                return;
            }

            // if they do enter some, go ahead and save it
            //first create a cItemCategory object using the constructor
            cItemCategory cat = new cItemCategory("0", txtCatName.Text, txtCatDescription.Text, cbxStatus.SelectedValue.ToString(), true);


            // check if category name already exists
            if (cat.CategoryCheck())
            {
                MessageBox.Show("This category already exists");
                txtCatName.Focus();
                txtCatName.SelectAll();
                return;
            }

            // this is the method to actually  save
            if (!cat.CategorySave())
            {
                MessageBox.Show("Could not save this category");
                return;
            }
            else
            {
                MessageBox.Show("Created Successfully.", "Adding Category");
                txtCatName.Clear();
                txtCatName.Focus();
                txtCatDescription.Clear();

            }
        }

        private void vAddCategory_Load(object sender, EventArgs e)
        {
            txtCatName.Clear();
        }
    }
}
