﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace EPSInventoryManager
    {
    public partial class frmReceiveStock : MetroForm
        {
        public frmReceiveStock()
            {
            InitializeComponent();
            }

        private void ReceiveStock_Load(object sender, EventArgs e)
            {
            // TODO: This line of code loads data into the 'ePS_ProjectDataSet.item' table. You can move, or remove it, as needed.
            this.itemTableAdapter.Fill(this.ePS_ProjectDataSet.item);
            // TODO: This line of code loads data into the 'ePS_ProjectDataSet.stock_transaction_detail' table. You can move, or remove it, as needed.
            
            }
        }
    }
