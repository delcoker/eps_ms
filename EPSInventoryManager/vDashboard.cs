﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace EPSInventoryManager
{
    public partial class frmDashboard : MetroForm
    {
        Timer t = new Timer();


        public frmDashboard()
        {
            InitializeComponent();
        }

        private void editCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void inventoryToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmInventory showInventory = new frmInventory();
            showInventory.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void metroTile1_Click(object sender, EventArgs e)
        {
            frmInventory showInventory = new frmInventory();
            showInventory.ShowDialog();

        }

        private void addCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void findCustomerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }


        private void metroTile3_Click(object sender, EventArgs e)
        {
            vReports showReports = new vReports();
            showReports.ShowDialog();
        }

        private void addItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void suppliersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //frmPartnerDetails showSuppliers = new frmPartnerDetails();
            //showSuppliers.Text = "Supplier Details";
            //showSuppliers.ShowDialog();
        }

        private void createCategoryToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void userToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmUser showUser = new frmUser();
            showUser.ShowDialog();
        }

        public void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //About showAbout = new About();
            //showAbout.ShowDialog();
            //MessageBox.Show(AppInfo().ToString(), "About:");
        }

        public string AppInfo()
        {
            string appName = "EPS-IMS version 1.0, Developed by:";
            string author = "David Asiamah and Del Coker.";
            string company = "@Ease-Solutions.";
            DateTime appDate = DateTime.Now;

            string about;
            about = appName + "\n" + author + "\n" + company + "\n" + appDate;
            return about;
        }

        private void customersToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void customersToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            //frmPartnerDetails showCustomers = new frmPartnerDetails();
            //showCustomers.Text = "Customer Details";
            //showCustomers.ShowDialog();
        }

        private void metroTile2_Click(object sender, EventArgs e)
        {
            vPartnerEdit pa = new vPartnerEdit();
            pa.ShowDialog();
        }

        private void editViewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            vCategoryEdit showCategoryEdit = new vCategoryEdit();
            showCategoryEdit.ShowDialog();
        }

        private void findItemToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void viewEditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            vCategoryEdit showCategoryEdit = new vCategoryEdit();
            showCategoryEdit.ShowDialog();
        }

        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void addToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmAddItem showItems = new frmAddItem();
            showItems.ShowDialog();
        }

        private void viewEditToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            vItemEdit it = new vItemEdit();
            it.ShowDialog();
        }

        private void addToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            vAddCategory showCategory = new vAddCategory();
            showCategory.ShowDialog();
        }

        private void addToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmPartnerDetails add = new frmPartnerDetails();
            add.ShowDialog();
        }

        private void viewEditToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            vPartnerEdit pa = new vPartnerEdit();
            pa.ShowDialog();
        }

        private void frmDashboard_Load(object sender, EventArgs e)
        {
            /**
            // TODO: This line of code loads data into the 'categoryChartDataSet.prc_CategoryCountReportGet' table. You can move, or remove it, as needed.
            this.prc_CategoryCountReportGetTableAdapter.Fill(this.categoryChartDataSet.prc_CategoryCountReportGet);
            // TODO: This line of code loads data into the 'chrtStCountDataSet.prc_StockCountReportGet' table. You can move, or remove it, as needed.
            this.prc_StockCountReportGetTableAdapter.Fill(this.chrtStCountDataSet.prc_StockCountReportGet);

            //Add data to chart
            chart1.DataBind();
            chart1.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            //chart1.Titles.Add("Item Stock Levels - YTD");
            chart1.Series["ItemCount"].XValueMember = "item_name";
            chart1.Series["ItemCount"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Auto;
            chart1.Series["ItemCount"].YValueMembers = "quantity_in_stock";
            chart1.Series["ItemCount"].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

            //Add Points to the Chart bars


            chart2.DataBind();
            chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            chart2.Series["ItemCategory"].XValueMember = "category_name";
            chart2.Series["ItemCategory"].XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Auto;
            chart2.Series["ItemCategory"].YValueMembers = "quantity_in_stock";
            chart2.Series["ItemCategory"].YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;

            //Set static values of the graph 
            //chart2.Series["Items"].Points.AddXY("O-Ring", 5000);
            //chart2.Series["Items"].Points.AddXY("Hammer", 3000);
            //chart2.Series["Items"].Points.AddXY("Rain Coat", 1500);
            //chart2.Series["Items"].Points.AddXY("Fork Lift", 4000);
            //chart2.Series["Items"].Points.AddXY("Compressors", 7000);


            //Add event handler to timer
            t.Tick += new EventHandler(this.timer1_Tick);
            t.Start();
            **/
        }

        private void frmDashboard_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void reportsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            vReports rpt = new vReports();
            rpt.ShowDialog();
        }

        private void companyProfileSetupToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void receivedStockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmInventory inv = new frmInventory();
            inv.ShowDialog();
        }
        //Running Clock for the application
        private void timer1_Tick(object sender, EventArgs e)
        {

            int hh = DateTime.Now.Hour;
            int mm = DateTime.Now.Minute;
            int ss = DateTime.Now.Second;

            string currentDate = DateTime.Now.Date.ToShortDateString();

            //time
            string time = "";

            //padding leading zero
            if (hh < 10)
            {
                time += "0" + hh;
            }
            else
            {
                time += hh;
            }

            time += ":";

            if (mm < 10)
            {
                time += "0" + mm;
            }
            else
            {
                time += mm;
            }

            time += ":";

            if (ss < 10)
            {
                time += "0" + ss;
            }
            else
            {
                time += ss;
            }

            //lblTime.Text = currentDate + "("+ time+")";
            lblTime.Text = DateTime.Today.ToLongDateString() + " (" + time + ")";
        }

        private void frmDashboard_Activated(object sender, EventArgs e)
        {
            //// TODO: This line of code loads data into the 'chrtStCountDataSet.prc_StockCountReportGet' table. You can move, or remove it, as needed.
            this.prc_StockCountReportGetTableAdapter.Fill(this.chrtStCountDataSet.prc_StockCountReportGet);
            //Refresh the chart to update data 
            //Note: Refresh() method does not show up in intelisense.
            chart1.Refresh();
        }
        vLogin showLogin = new vLogin();
        private void logOutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            showLogin.Show();
        }
    }
}

