﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;

using System.Threading.Tasks;
using System.Windows.Forms;

namespace EPSInventoryManager
{
    public partial class vReports : MetroFramework.Forms.MetroForm
    {
        private cReports rep;
        public vReports()
        {
            InitializeComponent();
            rep = new cReports();

            dtpStartDate.Format = DateTimePickerFormat.Custom;
            dtpStartDate.CustomFormat = "dddd dd MMM yyyy ";/*hh:mm:ss tt";*/

            dtpEndDate.Format = DateTimePickerFormat.Custom;
            dtpEndDate.CustomFormat = "dddd dd MMM yyyy"; /*hh:mm:ss tt";*/

        }

        private void vReport_Load(object sender, EventArgs e)
        {
            rptStockCount.Visible = false;
            rptStockTransactionDetail.Visible = false;
            // TODO: This line of code loads data into the 'reportStCountDataSet.prc_StockCountReportGet' table. You can move, or remove it, as needed.
            prc_StockCountReportGetTableAdapter.Fill(this.reportStCountDataSet.prc_StockCountReportGet);

            //rptStockCount.RefreshReport();
        }

        //Load Data into the Stock Count Report 
        private void FillStockLevelReport()
        {
            rptStockCount.BringToFront();

            prc_StockCountReportGetTableAdapter.Connection.ConnectionString = SqlCo.mConstr;
            prc_StockCountReportGetTableAdapter.Fill(reportStCountDataSet.prc_StockCountReportGet);


            // stackoverflow.com/  ------------ cant type everything out: set-page-layout-report-viewer-in-visual-studio-2010
            PageSettings pg = new PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 1;
            pg.Margins.Right = 0;
            PaperSize size = new PaperSize();
            size.RawKind = (int)PaperKind.A4;
            pg.PaperSize = size;
            rptStockCount.SetPageSettings(pg);

            rptStockCount.RefreshReport();
        }

        //Load data into the stock transaction detail report
        private void Fill_StockTrans_DetailReport()
        {
            rptStockTransactionDetail.BringToFront();

            prc_StockTransactionDetailReportGetTableAdapter.Connection.ConnectionString = SqlCo.mConstr;

            prc_StockTransactionDetailReportGetTableAdapter.Fill(reportStDetailDataSet.prc_StockTransactionDetailReportGet,
                rep.UserID, Convert.ToDateTime(rep.StartDate), Convert.ToDateTime(rep.EndDate));

            // stackoverflow.com/  ------------ cant type everything out: set-page-layout-report-viewer-in-visual-studio-2010
            PageSettings pg = new PageSettings();
            pg.Margins.Top = 0;
            pg.Margins.Bottom = 0;
            pg.Margins.Left = 0;
            pg.Margins.Right = 0;
            PaperSize size = new PaperSize();
            size.RawKind = (int)PaperKind.A4;
            pg.PaperSize = size;
            //rptStockTransactionDetail.SetPageSettings(pg);

            rptStockTransactionDetail.RefreshReport();
        }


        private void btnRunReport_Click(object sender, EventArgs e)
        {
            rep.UserID = "0";
            rep.StartDate = dtpStartDate.Value.ToString();
            rep.EndDate = dtpEndDate.Value.ToString();




            if (lstBoxReportType.SelectedItem != null && lstBoxReportType.SelectedItem.ToString().Equals("Stock Level Report"))
            {
                rptStockCount.Visible = true;
                rptStockCount.Dock = DockStyle.Fill;
                FillStockLevelReport();
            }
            else if (lstBoxReportType.SelectedItem != null && lstBoxReportType.SelectedItem.ToString().Equals("Stock Transaction Report"))
            {
                rptStockTransactionDetail.Visible = true;
                rptStockTransactionDetail.Dock = DockStyle.Fill;
                Fill_StockTrans_DetailReport();
            }
        }


    }
}
